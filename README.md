# movie

#### 介绍
爬虫 爬取豆瓣电影 完整系统  可选座 不能购票

#### 软件架构
软件架构说明


#### 安装教程

1.  拉取代码 并编译
2.  导入sql文件 在项目 doc下
3.  所需maven mysql

#### 使用说明

1.  管理员 和 用户都可通过登录页面进入不同的后台
2.  登录页面 localhost:8081/movie 账号密码:user user 管理员:admin amdin

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
