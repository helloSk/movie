package com.cinema.movie.controller;

import com.alibaba.druid.util.StringUtils;
import com.cinema.movie.entity.Hotmovie;
import com.cinema.movie.entity.MovieDetail;
import com.cinema.movie.entity.MoviePerformer;
import com.cinema.movie.service.HotmovieService;
import com.cinema.movie.service.MovieDetailService;
import com.cinema.movie.service.MoviePerformerService;
import com.cinema.movie.utils.HttpClientUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class AddHotMovieThread implements Runnable{

	private HotmovieService hotmovieService;
	private MovieDetailService movieDetailService;
	private MoviePerformerService moviePerformerService;
	private Hotmovie hotmovie;

	public AddHotMovieThread(HotmovieService hotmovieService, MovieDetailService movieDetailService, MoviePerformerService moviePerformerService,Hotmovie hotmovie) {
		this.hotmovieService = hotmovieService;
		this.movieDetailService = movieDetailService;
		this.moviePerformerService = moviePerformerService;
		this.hotmovie = hotmovie;
	}

	@Override
	public void run() {
		Hotmovie myHotMovie = this.hotmovie;
		if (StringUtils.isEmpty(myHotMovie.getRate())) {
			myHotMovie.setRate("暂无评分");
		}
		String movieId = myHotMovie.getId();
		myHotMovie.setPrice(price());
		String movieImg = myHotMovie.getCover();
		HttpClientUtil.sendGetAndSaveImg(movieImg);
		myHotMovie.setCover(realUrl(movieImg));
		hotmovieService.save(myHotMovie);
		StringBuilder getDetailUrl = new StringBuilder("https://movie.douban.com/subject/");
		getDetailUrl.append(movieId).append("/?from=showing");
		String detailHtml = HttpClientUtil.sendGet(getDetailUrl.toString());
		MovieDetail movieDetail = new MovieDetail();
		Document doc = Jsoup.parse(detailHtml);
		Element info = doc.getElementById("info");
		Elements attrs = info.getElementsByClass("attrs");
		movieDetail.setDirector(info.getElementsByAttributeValue("rel", "v:directedBy").text());
		if (attrs.size() > 2) {
			movieDetail.setWriter(attrs.get(1).text());
		}else{
			movieDetail.setWriter("无编剧");
		}
		movieDetail.setStar(info.getElementsByAttributeValue("rel", "v:starring").text());
		movieDetail.setType(info.getElementsByAttributeValue("property", "v:genre").text());
		movieDetail.setReleaseTime(info.getElementsByAttributeValue("property", "v:initialReleaseDate").text());
		movieDetail.setMovieTime(info.getElementsByAttributeValue("property", "v:runtime").text());
		Elements imdbSpan = info.getElementsByAttributeValue("rel", "nofollow");
		if (imdbSpan.size() > 1) {
			movieDetail.setImdb(imdbSpan.last().text());
		} else {
			movieDetail.setImdb(imdbSpan.text());
		}
		movieDetail.setPlot(doc.getElementById("link-report").text());
		movieDetail.setMovieId(movieId);
		movieDetail.setMovieName(myHotMovie.getTitle());
		movieDetail.setIsHot(1);
		movieDetailService.save(movieDetail);
		Elements celebritiesList = doc.getElementsByClass("celebrities-list");
		Elements celebritiesListLi = celebritiesList.get(0).select("li");
		celebritiesListLi.forEach(e -> {
			MoviePerformer moviePerformer = new MoviePerformer();
			String movieStarImg = getUrl(e.select("a").get(0).select("div").get(0).attr("style"));
			HttpClientUtil.sendGetAndSaveImg(movieStarImg);
			moviePerformer.setImg(realUrl(movieStarImg));
			moviePerformer.setMovieDuties(e.getElementsByClass("name").text());
			moviePerformer.setName(e.getElementsByClass("role").text());
			moviePerformer.setMovieId(movieId);
			moviePerformer.setIsHot(1);
			moviePerformerService.save(moviePerformer);
		});
	}

	public String price() {
		String price = null;
		String aString = String.valueOf(Math.random());
		for (int i = 0; i < aString.length(); i++) {
			if (!String.valueOf(aString.charAt(i)).equals("0") && !String.valueOf(aString.charAt(i)).equals(".")) {
				price = "3" + aString.charAt(i);
				break;
			}
		}
		return price;
	}

	private String getUrl(String url) {
		return url.substring(url.indexOf("(") + 1, url.indexOf(")"));
	}

	private String realUrl(String oldUrl) {
		String[] oldUrlS = oldUrl.split("/");
		return "movieFile/" + oldUrlS[oldUrlS.length - 1];
	}
}
