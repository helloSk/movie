package com.cinema.movie.controller;


import com.cinema.movie.entity.Apply;
import com.cinema.movie.entity.User;
import com.cinema.movie.service.ApplyService;
import com.cinema.movie.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hsk
 * @since 2019-02-23
 */
@Controller
public class ApplyController {

    @Autowired
    private HttpSession session;
    @Autowired
    private ApplyService applyService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private UserService userService;

    @RequestMapping("user/applyMember.do")
    public String applyMenber(){
        User user = (User) session.getAttribute("user");
        Apply apply = new Apply();
        apply.setUserId(user.getId());
        apply.setUserName(user.getUsername());
        user.setAdmin(3);
        userService.saveOrUpdate(user);
        applyService.save(apply);
        session.removeAttribute("user");
        session.setAttribute("user", user);
        return "redirect:/user/toHome";
    }

    @RequestMapping("admin/getApply.do")
    public String toApply(){
        List<Apply> applies = applyService.list();
        request.setAttribute("applies",applies);
        return "admin/apply";
    }


    @RequestMapping("admin/agreeApply.do")
    public String agreeApply(){
        String applyId = request.getParameter("applyId");
        applyService.removeById(applyId);
        String userId = request.getParameter("userId");
        User user = userService.getById(userId);
        user.setAdmin(4);
        userService.saveOrUpdate(user);
        return "redirect:/admin/getApply.do";
    }

}

