package com.cinema.movie.controller;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.movie.entity.Hotmovie;
import com.cinema.movie.entity.MovieDetail;
import com.cinema.movie.entity.MoviePerformer;
import com.cinema.movie.entity.Room;
import com.cinema.movie.service.HotmovieService;
import com.cinema.movie.service.MovieDetailService;
import com.cinema.movie.service.MoviePerformerService;
import com.cinema.movie.service.RoomService;
import com.cinema.movie.utils.FileUtil;
import com.cinema.movie.utils.HttpClientUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
@Controller
public class HotmovieController{

    @Autowired
    private HotmovieService hotmovieService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private MovieDetailService movieDetailService;
    @Autowired
    private MoviePerformerService moviePerformerService;

    private Hotmovie hotmovie;

    private  HotmovieController(){
    }

    public HotmovieController(Hotmovie hotmovie,HotmovieService hotmovieService, MovieDetailService movieDetailService, MoviePerformerService moviePerformerService) {
        this.hotmovie = hotmovie;
        this.hotmovieService = hotmovieService;
        this.movieDetailService = movieDetailService;
        this.moviePerformerService = moviePerformerService;
    }

    @RequestMapping("admin/addHot.do")
    public String addHot() {
        hotmovieService.deleteHot();
        QueryWrapper<MovieDetail> movieDetailQueryWrapper = new QueryWrapper<>();
        movieDetailQueryWrapper.lambda().eq(MovieDetail::getIsHot, 1);
        movieDetailService.remove(movieDetailQueryWrapper);
        QueryWrapper<MoviePerformer> moviePerformerQueryWrapper = new QueryWrapper<>();
        moviePerformerQueryWrapper.lambda().eq(MoviePerformer::getIsHot, 1);
        moviePerformerService.remove(moviePerformerQueryWrapper);
        FileUtil.cleanDirectory(new File(System.getProperty("user.dir") + "\\target\\classes\\static\\movieFile\\"));
        String result = HttpClientUtil.sendGet("https://movie.douban.com/j/search_subjects?type=movie&tag=%E7%83%AD%E9%97%A8&page_limit=50&page_start=0");
        Map aMap = JSON.parseObject(result, Map.class);
        String movies = aMap.get("subjects").toString();
        List<Hotmovie> list = JSON.parseArray(movies, Hotmovie.class);

        long startTime = System.currentTimeMillis();

        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>(100);
        ThreadPoolExecutor pool = new ThreadPoolExecutor(50, 100, 10, TimeUnit.MINUTES, queue);
        for (int i = 0; i < list.size(); i++) {
            AddHotMovieThread addHotMovieThread = new AddHotMovieThread(hotmovieService,movieDetailService,moviePerformerService,list.get(i));
            pool.execute(addHotMovieThread);
        }
        while (true){
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            int count = pool.getActiveCount();
            if (count == 0){
                break;
            }
        }
        System.out.println("花费:"+(System.currentTimeMillis()-startTime)+"毫秒");
        return "redirect:/admin/getHot.do";
    }

    @RequestMapping("admin/getHot.do")
    public String getHot() {
        List<Hotmovie> hotMovies = hotmovieService.getAllHotMovie(null);
        request.setAttribute("hotMovies", hotMovies);
        return "admin/hot_movie";
    }

    @RequestMapping("admin/deleteHotById.do")
    public String deleteHotById() {
        String movieId = request.getParameter("movieId");
        hotmovieService.removeById(movieId);
        return "redirect:/admin/getHot.do";
    }


    @RequestMapping("admin/toAddRoom.do")
    public String toRoom() {
        String movieId = request.getParameter("movieId");
        Hotmovie hotmovie = hotmovieService.getById(movieId);
        request.setAttribute("movieName", hotmovie.getTitle());
        request.setAttribute("movieId", movieId);
        QueryWrapper<Room> roomQueryWrapper = new QueryWrapper<>();
        roomQueryWrapper.eq("movieId", movieId);
        List<Room> rooms = roomService.list(roomQueryWrapper);
        request.setAttribute("rooms", rooms);
        return "admin/movie_room";
    }

}

