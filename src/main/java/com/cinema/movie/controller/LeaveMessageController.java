package com.cinema.movie.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.movie.entity.Hotmovie;
import com.cinema.movie.entity.LeaveMessage;
import com.cinema.movie.service.HotmovieService;
import com.cinema.movie.service.LeaveMessageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hsk
 * @since 2019-02-22
 */
@Controller
public class LeaveMessageController {

    @Autowired
    private LeaveMessageService leaveMessageService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HotmovieService hotmovieService;

    @RequestMapping("user/toLeaveMessage.do")
    public String toLeaveMessage(){
        String movieId = request.getParameter("movieId");
        QueryWrapper<LeaveMessage> leaveMessageQueryWrapper = new QueryWrapper<>();
        leaveMessageQueryWrapper.lambda().eq(LeaveMessage::getMovieId, movieId);
        List<LeaveMessage> leaveMessages = leaveMessageService.list(leaveMessageQueryWrapper);
        Hotmovie hotmovie = hotmovieService.getById(movieId);
        request.setAttribute("leaveMessages",leaveMessages);
        request.setAttribute("hotmovie",hotmovie);
        return "user/leave_message";
    }

    @RequestMapping("user/postMessage.do")
    public String saveMessage(LeaveMessage leaveMessage){
        leaveMessageService.save(leaveMessage);
        return "redirect:/user/toLeaveMessage.do?movieId="+leaveMessage.getMovieId()+"";
    }

}

