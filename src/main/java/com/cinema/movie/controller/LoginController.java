package com.cinema.movie.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cinema.movie.entity.Hotmovie;
import com.cinema.movie.entity.Recentry;
import com.cinema.movie.entity.User;
import com.cinema.movie.service.HotmovieService;
import com.cinema.movie.service.RecentryService;
import com.cinema.movie.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private UserService userService;
    @Autowired
    private HttpSession session;
    @Autowired
    private HotmovieService hotmovieService;
    @Autowired
    private RecentryService recentryService;

    @RequestMapping("login")
    public String index(){
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String type = request.getParameter("type");
        User user = userService.login(username, password);
        if (ObjectUtils.isEmpty(user)){
            request.setAttribute("message","账号或密码错误");
            if (type.equals("1")){
                return "admin/index";
            }else{
                return "user/index";
            }
        }
        session.setAttribute("user",user);
        if (user.getAdmin() == 1){
            return "admin/home";
        }else{
            return "redirect:user/toHome";
        }
    }

    @RequestMapping("user/toHome")
    public String toHome(){
        String movieName = request.getParameter("movieName");
        request.setAttribute("movieName",movieName);
        List<Hotmovie> hotMovies =  hotmovieService.getAllHotMovie(movieName);
        List<Recentry> recentryMovies = recentryService.list();
        request.setAttribute("recentryMovies",recentryMovies);
        request.setAttribute("hotMovies", hotMovies);
        return "user/home";
    }

    @RequestMapping("regist")
    public String regist(User user){
        user.setDateTime(new Date());
        userService.save(user);
        return "user/index";
    }


    @RequestMapping("user/rank.do")
    public String rank(){
        List<Map> hotMovies =  hotmovieService.getHotMovieRank();
        request.setAttribute("hotMovies",hotMovies);
        return "user/ranking";
    }


    /**
     * 默认访问路径
     * @return
     */
    @RequestMapping("")
    public String login(){
        return "user/index";
    }

    @RequestMapping("toLogin")
    public String toAdminLogin(){
        return "admin/index";
    }

    @GetMapping("logout.do")
    public String logOut(){
        User user = (User) session.getAttribute("user");
        if (ObjectUtils.isEmpty(user)){
            System.out.println("登录已失效，默认跳转用户登录页面");
            return "user/index";
        }
        session.removeAttribute("user");
        if (user.getAdmin() == 1){
            return "admin/index";
        }else{
            return "user/index";
        }
    }

    @RequestMapping("user/logout.do")
    public String userLogOut(){
        return "redirect:/logout.do";
    }

}
