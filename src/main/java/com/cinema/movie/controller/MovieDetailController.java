package com.cinema.movie.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.movie.entity.Hotmovie;
import com.cinema.movie.entity.MovieDetail;
import com.cinema.movie.entity.MoviePerformer;
import com.cinema.movie.entity.Recentry;
import com.cinema.movie.service.HotmovieService;
import com.cinema.movie.service.MovieDetailService;
import com.cinema.movie.service.MoviePerformerService;
import com.cinema.movie.service.RecentryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hsk
 * @since 2019-02-23
 */
@Controller
public class MovieDetailController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private MovieDetailService movieDetailService;
    @Autowired
    private HotmovieService hotmovieService;
    @Autowired
    private RecentryService recentryService;
    @Autowired
    private MoviePerformerService moviePerformerService;

    @RequestMapping("user/toMovieDetail")
    public String toMovieDetail(){
        String movieId = request.getParameter("movieId");
        QueryWrapper<Hotmovie> hotMovieQueryWrapper = new QueryWrapper<>();
        hotMovieQueryWrapper.lambda().eq(Hotmovie::getId,movieId);
        Hotmovie hotmovie = hotmovieService.getOne(hotMovieQueryWrapper);
        QueryWrapper<MovieDetail> movieDetailQueryWrapper = new QueryWrapper<>();
        movieDetailQueryWrapper.lambda().eq(MovieDetail::getMovieId, movieId);
        MovieDetail movieDetail = movieDetailService.getOne(movieDetailQueryWrapper);
        List<Recentry> recentryMovies = recentryService.list();
        QueryWrapper<MoviePerformer> moviePerformerQueryWrapper = new QueryWrapper<>();
        moviePerformerQueryWrapper.lambda().eq(MoviePerformer::getMovieId, movieId);
        List<MoviePerformer> moviePerformers = moviePerformerService.list(moviePerformerQueryWrapper);
        request.setAttribute("moviePerformers",moviePerformers);
        request.setAttribute("recentryMovies",recentryMovies);
        request.setAttribute("hotMovie",hotmovie);
        request.setAttribute("movieDetail", movieDetail);
        return "user/movie_details";
    }

}

