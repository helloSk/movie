package com.cinema.movie.controller;


import com.cinema.movie.entity.Recentry;
import com.cinema.movie.service.RecentryService;
import com.cinema.movie.utils.HttpClientUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
@Controller
public class RecentryController {

    @Autowired
    private RecentryService recentryService;
    @Autowired
    private HttpServletRequest request;

    @RequestMapping("admin/addRecentry.do")
    public String addRecentry() {
        recentryService.remove(null);
        String result = HttpClientUtil.sendGet("https://movie.douban.com/");
        Document doc = Jsoup.parse(result);
        Elements screeningBd = doc.getElementsByClass("screening-bd");
        if (screeningBd.size()!=0) {
            Elements uiSlideItem = screeningBd.get(0).getElementsByClass("ui-slide-item");
            for (int i = 0; i < uiSlideItem.size(); i++) {
                if (uiSlideItem.get(i) != null && !uiSlideItem.get(i).text().equals("")) {
                    Elements img = uiSlideItem.get(i).select("img");
                    String[] url = uiSlideItem.get(i).attr("data-trailer").split("/");
                    Recentry recentry = new Recentry();
                    recentry.setId(url[url.length-2]);
                    recentry.setCover(img.get(0).attr("src"));
                    recentry.setTitle(uiSlideItem.get(i).attr("data-title"));
                    if (uiSlideItem.get(i).attr("data-rate")== null||uiSlideItem.get(i).attr("data-rate").trim().equals("")) {
                        recentry.setRate("暂无评分");
                    }else {
                        recentry.setRate(uiSlideItem.get(i).attr("data-rate"));
                    }
                    recentryService.save(recentry);
                }
            }
        }
        return "redirect:/admin/getRecentry.do";
    }

    @RequestMapping("admin/getRecentry.do")
    public String getRecentry(){
        List<Recentry> recentryMovies = recentryService.list();
        request.setAttribute("recentryMovies",recentryMovies);
        return "admin/recentry";
    }

    @RequestMapping("admin/deleteRecentryById.do")
    public String deleteRecentryById(){
        String movieId = request.getParameter("movieId");
        recentryService.removeById(movieId);
        return "redirect:/admin/getRecentry.do";
    }

}

