package com.cinema.movie.controller;


import javax.servlet.http.HttpServletRequest;

import com.cinema.movie.entity.Room;
import com.cinema.movie.service.RoomService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
@Controller
public class RoomController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private RoomService roomService;

    @RequestMapping("admin/addRoom.do")
    public String toRoom(){
        String movieId = request.getParameter("movieId");
        String movieName = request.getParameter("movieName");
        String roomName = request.getParameter("roomName");
        String price = request.getParameter("price");
        String mprice = request.getParameter("mprice");
        String seatSize = request.getParameter("seatSize");
        Room room = new Room();
        room.setMovieId(movieId);
        room.setMovieName(movieName);
        room.setName(roomName);
        room.setSeatSize(Integer.valueOf(seatSize));
        room.setPrice(Integer.valueOf(price));
        room.setMprice(Integer.valueOf(mprice));
        roomService.save(room);
        return "redirect:/admin/toAddRoom.do?movieId="+movieId+"&&movieName="+movieName+"";
    }

    @RequestMapping("admin/deleteRoomById.do")
    public String deleteRoomById(){
        String roomId = request.getParameter("roomId");
        String movieId = request.getParameter("movieId");
        String movieName = request.getParameter("movieName");
        roomService.removeById(roomId);
        return "redirect:/admin/toAddRoom.do?movieId="+movieId+"&&movieName="+movieName+"";
    }

}

