package com.cinema.movie.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.movie.entity.Apply;
import com.cinema.movie.entity.Hotmovie;
import com.cinema.movie.entity.Recentry;
import com.cinema.movie.entity.Room;
import com.cinema.movie.entity.Seat;
import com.cinema.movie.entity.User;
import com.cinema.movie.service.ApplyService;
import com.cinema.movie.service.HotmovieService;
import com.cinema.movie.service.RecentryService;
import com.cinema.movie.service.RoomService;
import com.cinema.movie.service.SeatService;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
@Controller
public class SeatController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private RecentryService recentryService;
    @Autowired
    private HotmovieService hotmovieService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private SeatService seatService;
    @Autowired
    private ApplyService applyService;
    @Autowired
    private HttpSession session;

    @RequestMapping("user/toSeat.do")
    public String toSeat(){
        String movieId = request.getParameter("movieId");
        String roomId = request.getParameter("roomId");
        String price = request.getParameter("price");
        if (StringUtils.isNotBlank(movieId)){
            List<Recentry> recentryMovies = recentryService.list();
            request.setAttribute("recentryMovies",recentryMovies);
            Hotmovie hotmovie = hotmovieService.getById(movieId);
            request.setAttribute("hotMovie",hotmovie);
            QueryWrapper<Room> roomQueryWrapper = new QueryWrapper<>();
            roomQueryWrapper.eq("movieId",movieId);
            List<Room> rooms = roomService.list(roomQueryWrapper);
            request.setAttribute("rooms",rooms);
            //如果没有影厅 则创建一个
            if (rooms.size() == 0){
                Room room = new Room();
                room.setName("一号影厅");
                room.setMovieName(hotmovie.getTitle());
                room.setMovieId(movieId);
                room.setPrice(35);
                room.setMprice(30);
                room.setSeatSize(150);
                roomService.save(room);
            }
            rooms = roomService.list(roomQueryWrapper);
            if (StringUtils.isBlank(roomId) && ObjectUtils.allNotNull(rooms)){
                roomId = rooms.get(0).getId();
                User user = (User) session.getAttribute("user");
                if (user.getAdmin() == 4){
                    price = String.valueOf(rooms.get(0).getMprice());
                }else{
                    price = String.valueOf(rooms.get(0).getPrice());
                }
            }
            Room room1 = roomService.getById(roomId);
            request.setAttribute("roomName",room1.getName());
            request.setAttribute("roomId",roomId);
            request.setAttribute("room", room1);
            request.setAttribute("price",price);
            QueryWrapper<Seat> seatQueryWrapper = new QueryWrapper<>();
            seatQueryWrapper.eq("roomId", roomId);
            seatQueryWrapper.eq("movieId", movieId);
            List<Seat> movieSeats = seatService.list(seatQueryWrapper);
            User user = (User) session.getAttribute("user");
            QueryWrapper<Apply> applyQueryWrapper = new QueryWrapper<>();
            applyQueryWrapper.lambda().eq(Apply::getUserId, user.getId());
            List<Apply> applies = applyService.list(applyQueryWrapper);
            request.setAttribute("applies", applies);
            request.setAttribute("movieSeats",movieSeats);
        }
        return "user/seat";
    }

    @RequestMapping("user/addSeat.do")
    @ResponseBody
    public List<Seat> addSeat(){
        String userTelephone = request.getParameter("userTelephone");
        String number = request.getParameter("number");
        String movieId = request.getParameter("movieId");
        String movieName = request.getParameter("movieName");
        String userId = request.getParameter("userId");
        String roomId = request.getParameter("roomId");
        String roomName = request.getParameter("roomName");
        String userName = request.getParameter("userName");
        String price = request.getParameter("price");
        Seat seat = new Seat();
        seat.setMovieId(movieId);
        seat.setMovieName(movieName);
        seat.setNumber(number);
        seat.setRoomId(roomId);
        seat.setUserId(userId);
        seat.setState(0);
        seat.setUserTelephone(userTelephone);
        seat.setRoomName(roomName);
        seat.setUserName(userName);
        seat.setPrice(Integer.valueOf(price));
        seatService.save(seat);
//        return "redirect:/user/toSeat.do?movieId="+movieId+"&&roomId="+roomId+"";
        Map<String, String> map = new HashMap<>();
        List<Seat> seats = new ArrayList<>();
        QueryWrapper<Seat> seatQueryWrapper = new QueryWrapper<>();
        seatQueryWrapper.lambda().eq(Seat::getRoomId, roomId);
        seats = seatService.list(seatQueryWrapper);
        return seats;
    }

    @RequestMapping("user/deleteSeat.do")
    public String deleteSeat(){
        String seatId = request.getParameter("seatId");
        seatService.removeById(seatId);
        return "redirect:/user/toMyMovieSeat.do";
    }

    @RequestMapping("user/sendOrder.do")
    public String sendOrder(){
        String seatId = request.getParameter("seatId");
        QueryWrapper<Seat> seatQueryWrapper = new QueryWrapper<>();
        seatQueryWrapper.lambda().eq(Seat::getId, seatId);
        Seat seat = seatService.getOne(seatQueryWrapper);
        seat.setState(1);
        seatService.saveOrUpdate(seat);
        return "redirect:/user/toMyMovieSeat.do";
    }

    @RequestMapping("admin/getMovieSeat.do")
    public String toMovieSeat(){
        QueryWrapper<Seat> seatQueryWrapper = new QueryWrapper<>();
        seatQueryWrapper.lambda().eq(Seat::getState, 1);
        List<Seat> movieSeats = seatService.list(seatQueryWrapper);
        request.setAttribute("movieSeats",movieSeats);
        return "admin/movie_seat";
    }

}

