package com.cinema.movie.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.movie.entity.Seat;
import com.cinema.movie.entity.User;
import com.cinema.movie.service.SeatService;
import com.cinema.movie.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HttpSession session;
    @Autowired
    private SeatService seatService;

    @RequestMapping("admin/getUser.do")
    public String getUser(){
        List<User> users = userService.list();
        request.setAttribute("users", users);
        return "admin/user";
    }

    @RequestMapping("admin/password.do")
    public String toPassword(){
        return "admin/password";
    }

    @RequestMapping("admin/updatePassword.do")
    public String updatePassword(){
        User user = new User();
        String password = request.getParameter("password");
        User thisUser = (User) session.getAttribute("user");
        if (!password.equals(thisUser.getPassword())){
            request.setAttribute("message", "旧密码输入错误");
            return "admin/password";
        }
        String newPassword = request.getParameter("newPassword");
        String id = request.getParameter("id");
        user.setId(id);
        user.setPassword(newPassword);
        userService.updateById(user);
        thisUser.setPassword(newPassword);
        session.setAttribute("user",thisUser);
        return "admin/password_ok";
    }

    @RequestMapping("user/toMyMovieSeat.do")
    public String toMyMovieSeat(){
        String userId = request.getParameter("userId");
        if (StringUtils.isBlank(userId)){
            User user = (User) session.getAttribute("user");
            userId = user.getId();
        }
        QueryWrapper<Seat> seatQueryWrapper = new QueryWrapper<>();
        seatQueryWrapper.eq("userId",userId);
        List<Seat> movieSeats = seatService.list(seatQueryWrapper);
        request.setAttribute("movieSeats",movieSeats);
        return "user/my_movie_seat";
    }

    @RequestMapping("user/updatePassword")
    public String userUpdatePassword(){
        String newPassword = request.getParameter("newPassword");
        User user = (User) session.getAttribute("user");
        user.setPassword(newPassword);
        userService.saveOrUpdate(user);
        return "redirect:/user/toHome";
    }

}

