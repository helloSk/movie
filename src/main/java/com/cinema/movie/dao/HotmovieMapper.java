package com.cinema.movie.dao;

import java.util.List;
import java.util.Map;

import com.cinema.movie.entity.Hotmovie;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
public interface HotmovieMapper extends BaseMapper<Hotmovie> {

	List<Map> getHotMovieRank();

}
