package com.cinema.movie.dao;

import com.cinema.movie.entity.MovieDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hsk
 * @since 2019-02-23
 */
public interface MovieDetailMapper extends BaseMapper<MovieDetail> {

}
