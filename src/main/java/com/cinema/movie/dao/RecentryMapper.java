package com.cinema.movie.dao;

import com.cinema.movie.entity.Recentry;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
public interface RecentryMapper extends BaseMapper<Recentry> {

}
