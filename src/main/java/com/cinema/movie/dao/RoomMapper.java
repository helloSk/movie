package com.cinema.movie.dao;

import com.cinema.movie.entity.Room;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
public interface RoomMapper extends BaseMapper<Room> {

}
