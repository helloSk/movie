package com.cinema.movie.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cinema.movie.entity.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
public interface UserMapper extends BaseMapper<User> {

}
