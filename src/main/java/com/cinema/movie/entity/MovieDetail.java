package com.cinema.movie.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author hsk
 * @since 2019-02-23
 */
@TableName("m_movie_detail")
public class MovieDetail extends Model<MovieDetail> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    @TableField("movie_id")
    private String movieId;

    @TableField("director")
    private String director;

    @TableField("writer")
    private String writer;

    @TableField("star")
    private String star;

    @TableField("type")
    private String type;

    @TableField("imdb")
    private String imdb;

    @TableField("release_time")
    private String releaseTime;

    @TableField("movie_time")
    private String movieTime;

    @TableField("plot")
    private String plot;

    @TableField("movie_name")
    private String movieName;

    @TableField("is__hot")
    private Integer isHot;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }

    public String getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getMovieTime() {
        return movieTime;
    }

    public void setMovieTime(String movieTime) {
        this.movieTime = movieTime;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MovieDetail{" +
        "id=" + id +
        ", movieId=" + movieId +
        ", director=" + director +
        ", writer=" + writer +
        ", star=" + star +
        ", type=" + type +
        ", imdb=" + imdb +
        ", releaseTime=" + releaseTime +
        ", movieTime=" + movieTime +
        ", plot=" + plot +
        ", movieName=" + movieName +
        ", isHot=" + isHot +
        "}";
    }
}
