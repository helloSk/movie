package com.cinema.movie.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author hsk
 * @since 2019-02-23
 */
@TableName("m_movie_performer")
public class MoviePerformer extends Model<MoviePerformer> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    @TableField("name")
    private String name;

    @TableField("movie_duties")
    private String movieDuties;

    @TableField("img")
    private String img;

    @TableField("movie_id")
    private String movieId;

    @TableField("is_hot")
    private Integer isHot;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMovieDuties() {
        return movieDuties;
    }

    public void setMovieDuties(String movieDuties) {
        this.movieDuties = movieDuties;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MoviePerformer{" +
        "id=" + id +
        ", name=" + name +
        ", movieDuties=" + movieDuties +
        ", img=" + img +
        ", movieId=" + movieId +
        ", isHot=" + isHot +
        "}";
    }
}
