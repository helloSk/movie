package com.cinema.movie.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cinema.movie.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Component
@WebFilter(filterName="MyFilter",urlPatterns="/*")
public class MyFilter implements Filter {

    @Autowired
    private HttpSession session;

    @Override
    public void init(FilterConfig filterConfig) {
        System.out.println("过滤器初始化");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String url = String.valueOf(request.getRequestURL());
        if (url.contains("/login")|| url.contains("/regist")|| url.contains("toLogin") || url.contains("logout") || url.substring(url.length()-7).equals("/movie/") || url.contains(".js") || url.contains(".css")){
            filterChain.doFilter(request, response);
        }else {
            User user = (User) session.getAttribute("user");
            if (ObjectUtils.isEmpty(user)){
                response.sendRedirect("/movie/");
            }else{
                filterChain.doFilter(request, response);
            }
        }
    }

    @Override
    public void destroy() {
        System.out.println("过滤器销毁");
    }

}
