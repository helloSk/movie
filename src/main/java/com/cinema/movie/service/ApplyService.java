package com.cinema.movie.service;

import com.cinema.movie.entity.Apply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hsk
 * @since 2019-02-23
 */
public interface ApplyService extends IService<Apply> {

}
