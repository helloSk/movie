package com.cinema.movie.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cinema.movie.entity.Hotmovie;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
public interface HotmovieService extends IService<Hotmovie> {

    List<Hotmovie> getAllHotMovie(String movieName);

    void deleteHot();

    List<Map> getHotMovieRank();

}
