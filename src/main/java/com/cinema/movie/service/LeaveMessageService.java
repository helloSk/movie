package com.cinema.movie.service;

import com.cinema.movie.entity.LeaveMessage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hsk
 * @since 2019-02-22
 */
public interface LeaveMessageService extends IService<LeaveMessage> {

}
