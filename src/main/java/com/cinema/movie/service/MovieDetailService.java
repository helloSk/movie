package com.cinema.movie.service;

import com.cinema.movie.entity.MovieDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hsk
 * @since 2019-02-23
 */
public interface MovieDetailService extends IService<MovieDetail> {

}
