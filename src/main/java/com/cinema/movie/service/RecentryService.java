package com.cinema.movie.service;

import com.cinema.movie.entity.Recentry;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
public interface RecentryService extends IService<Recentry> {

}
