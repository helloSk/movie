package com.cinema.movie.service;

import com.cinema.movie.entity.Room;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
public interface RoomService extends IService<Room> {

}
