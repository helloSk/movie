package com.cinema.movie.service;

import com.cinema.movie.entity.Seat;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
public interface SeatService extends IService<Seat> {

}
