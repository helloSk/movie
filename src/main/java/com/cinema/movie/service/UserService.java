package com.cinema.movie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cinema.movie.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
public interface UserService extends IService<User> {

    User login(String username, String password);
}
