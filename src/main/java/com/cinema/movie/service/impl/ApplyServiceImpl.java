package com.cinema.movie.service.impl;

import com.cinema.movie.entity.Apply;
import com.cinema.movie.dao.ApplyMapper;
import com.cinema.movie.service.ApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hsk
 * @since 2019-02-23
 */
@Service
public class ApplyServiceImpl extends ServiceImpl<ApplyMapper, Apply> implements ApplyService {

}
