package com.cinema.movie.service.impl;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.movie.dao.HotmovieMapper;
import com.cinema.movie.entity.Hotmovie;
import com.cinema.movie.service.HotmovieService;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
@Service
public class HotmovieServiceImpl extends ServiceImpl<HotmovieMapper, Hotmovie> implements HotmovieService {

    @Autowired
    private HotmovieMapper hotmovieMapper;

    @Override
    public List<Hotmovie> getAllHotMovie(String movieName) {
        QueryWrapper<Hotmovie> hotmovieQueryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(movieName)){
            hotmovieQueryWrapper.lambda().like(Hotmovie::getTitle, movieName);
        }
//        hotmovieQueryWrapper.lambda().orderByDesc(Hotmovie::getRate);
        return hotmovieMapper.selectList(hotmovieQueryWrapper);
    }

    @Override
    public void deleteHot() {
        hotmovieMapper.delete(null);
    }

    @Override
    public List<Map> getHotMovieRank() {
         return hotmovieMapper.getHotMovieRank();
    }
}
