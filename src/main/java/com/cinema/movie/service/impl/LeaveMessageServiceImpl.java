package com.cinema.movie.service.impl;

import com.cinema.movie.entity.LeaveMessage;
import com.cinema.movie.dao.LeaveMessageMapper;
import com.cinema.movie.service.LeaveMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hsk
 * @since 2019-02-22
 */
@Service
public class LeaveMessageServiceImpl extends ServiceImpl<LeaveMessageMapper, LeaveMessage> implements LeaveMessageService {

}
