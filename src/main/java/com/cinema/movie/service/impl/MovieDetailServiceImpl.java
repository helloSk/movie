package com.cinema.movie.service.impl;

import com.cinema.movie.entity.MovieDetail;
import com.cinema.movie.dao.MovieDetailMapper;
import com.cinema.movie.service.MovieDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hsk
 * @since 2019-02-23
 */
@Service
public class MovieDetailServiceImpl extends ServiceImpl<MovieDetailMapper, MovieDetail> implements MovieDetailService {

}
