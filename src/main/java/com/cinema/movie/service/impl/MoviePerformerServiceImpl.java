package com.cinema.movie.service.impl;

import com.cinema.movie.entity.MoviePerformer;
import com.cinema.movie.dao.MoviePerformerMapper;
import com.cinema.movie.service.MoviePerformerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hsk
 * @since 2019-02-23
 */
@Service
public class MoviePerformerServiceImpl extends ServiceImpl<MoviePerformerMapper, MoviePerformer> implements MoviePerformerService {

}
