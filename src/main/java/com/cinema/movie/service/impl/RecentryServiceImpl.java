package com.cinema.movie.service.impl;

import com.cinema.movie.entity.Recentry;
import com.cinema.movie.dao.RecentryMapper;
import com.cinema.movie.service.RecentryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
@Service
public class RecentryServiceImpl extends ServiceImpl<RecentryMapper, Recentry> implements RecentryService {

}
