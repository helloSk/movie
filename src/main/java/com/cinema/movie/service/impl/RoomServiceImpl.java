package com.cinema.movie.service.impl;

import com.cinema.movie.entity.Room;
import com.cinema.movie.dao.RoomMapper;
import com.cinema.movie.service.RoomService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
@Service
public class RoomServiceImpl extends ServiceImpl<RoomMapper, Room> implements RoomService {

}
