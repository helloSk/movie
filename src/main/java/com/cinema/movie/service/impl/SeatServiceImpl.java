package com.cinema.movie.service.impl;

import com.cinema.movie.entity.Seat;
import com.cinema.movie.dao.SeatMapper;
import com.cinema.movie.service.SeatService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hsk
 * @since 2019-01-30
 */
@Service
public class SeatServiceImpl extends ServiceImpl<SeatMapper, Seat> implements SeatService {

}
