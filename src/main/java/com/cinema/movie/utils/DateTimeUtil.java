package com.cinema.movie.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间类
 */
public class DateTimeUtil {

	/**
	 * 获取当前时间
	 * @return TimesTamp
	 */
	public static Timestamp getTime() {
		Date date = new Date();
		Timestamp datetime = new Timestamp(date.getTime());
		return datetime;
	}

	public static Date formatDate(String s) {
		if (s == null || s.isEmpty()) {
			s = "0000-00-00 00:00";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date newDate = null;
		try {
			return sdf.parse(s);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return newDate;
	}

	public static int daysOfTwo(Date startDate, Date endDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		int day1 = calendar.get(Calendar.DAY_OF_YEAR);
		calendar.setTime(endDate);
		int day2 = calendar.get(Calendar.DAY_OF_YEAR);
		return day2 - day1;

	}

}
