package com.cinema.movie.utils;

import org.apache.tomcat.util.http.fileupload.FileUtils;

import java.io.*;

/**
 * 文件工具类
 * @author MrHu
 */
public class FileUtil {

    /**
     * 保存文件
     * @param inputStream
     * @param fileName 文件路径
     */
    public static void savePic(InputStream inputStream, String fileName) {
        OutputStream os = null;
        try {
            String path = System.getProperty("user.dir") + "\\target\\classes\\static\\movieFile\\";
            // 2、保存到临时文件
            // 1K的数据缓冲
            byte[] bs = new byte[1024];
            // 读取到的数据长度
            int len;
            // 输出的文件流保存到本地文件

            File tempFile = new File(path);
            if (!tempFile.exists()) {
                tempFile.mkdirs();
            }
            os = new FileOutputStream(tempFile.getPath() + File.separator + fileName);
            // 开始读取
            while ((len = inputStream.read(bs)) != -1) {
                os.write(bs, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 完毕，关闭所有链接
            try {
                os.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 删除文件夹下所有文件
     * @param directory
     */
    public static void cleanDirectory(File directory){
        try {
            FileUtils.cleanDirectory(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除文件夹即文件夹中文件
     * @param directory
     */
    public static void deleteDirectory(File directory){
        try {
            FileUtils.deleteDirectory(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
