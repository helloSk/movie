package com.cinema.movie.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

public class HttpClientUtil {

    // 默认字符集
    private static final String ENCODING = "UTF-8";

    /**
     * @Title: sendPost
     * @Description: TODO(发送post请求)
     * @param url 请求地址
     * @param headers 请求头
     * @param data 请求实体
     * @param encoding 字符集
     * @author MrHu
     * @return String
     * @throws
     */
    public static String sendPost(String url,Map<String, String> headers, JSONObject data, String encoding) {
        // 请求返回结果
        String resultJson = null;
        // 创建Client
        CloseableHttpClient client = HttpClients.createDefault();
        // 创建HttpPost对象
        HttpPost httpPost = new HttpPost();

        try {
            postData(url, headers, httpPost, data);
            // 发送请求,返回响应对象
            CloseableHttpResponse response = client.execute(httpPost);
            // 获取响应状态
            int status = response.getStatusLine().getStatusCode();
            if (status == HttpStatus.SC_OK) {
                // 获取响应结果
                resultJson = EntityUtils.toString(response.getEntity(), encoding);
            } else {
                System.out.println("响应失败，状态码：" + status);
            }

        } catch (Exception e) {
            System.out.println("发送post请求失败:" +  e);
        } finally {
            httpPost.releaseConnection();
        }
        return resultJson;
    }

    /**
     * @Title: sendPost
     * @Description: TODO(发送post请求)
     * @param url 请求地址
     * @param headers 请求头
     * @param data 请求实体
     * @author MrHu
     * @return String
     * @throws
     */
    public static InputStream sendPostForInputStream(String url,Map<String, String> headers, JSONObject data) {
        // 请求返回结果
        InputStream resultJson = null;
        // 创建Client
        CloseableHttpClient client = HttpClients.createDefault();
        // 创建HttpPost对象
        HttpPost httpPost = new HttpPost();
        try {
            postData(url, headers, httpPost, data);
            // 发送请求,返回响应对象
            CloseableHttpResponse response = client.execute(httpPost);
            // 获取响应状态
            int status = response.getStatusLine().getStatusCode();
            if (status == HttpStatus.SC_OK) {
                // 获取响应结果
                resultJson = response.getEntity().getContent();
            } else {
                System.out.println("响应失败，状态码：" + status);
            }

        } catch (Exception e) {
            System.out.println("发送post请求失败:" +  e);
        } finally {
            httpPost.releaseConnection();
        }
        return resultJson;
    }


    /**
     * post请求数据
     * @param url
     * @param headers
     * @param httpPost
     * @param data
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     */
    private static void postData(String url, Map<String, String> headers, HttpPost httpPost, JSONObject data) throws URISyntaxException, UnsupportedEncodingException {
        // 设置请求地址
        httpPost.setURI(new URI(url));
        // 设置请求头
        if (headers != null) {
            setHeaders(httpPost, new BasicHeader[headers.size()],headers);
        }
        // 设置实体
        httpPost.setEntity(new StringEntity(JSON.toJSONString(data)));
    }

    /**
     * 设置头
     * @param httpPost
     * @param allHeader
     * @param headers
     */
    private static void setHeaders(HttpPost httpPost,Header[] allHeader,Map<String, String> headers){
        int i = 0;
        for (Map.Entry<String, String> entry: headers.entrySet()){
            allHeader[i] = new BasicHeader(entry.getKey(), entry.getValue());
            i++;
        }
        httpPost.setHeaders(allHeader);
    }

    /**
     * @Title: sendPost
     * @Description: TODO(发送post请求，请求数据默认使用json格式，默认使用UTF-8编码)
     * @param url 请求地址
     * @param data 请求实体
     * @author MrHu
     * @return String
     * @throws
     */
    public static String sendPost(String url, JSONObject data) {
        // 设置默认请求头
        Map<String, String> headers = new HashMap<>();
        headers.put("content-type", "application/json");

        return sendPost(url, headers, data, ENCODING);
    }

    /**
     * @Title: sendPost
     * @Description: TODO(发送post请求，请求数据默认使用json格式，默认使用UTF-8编码)
     * @param url 请求地址
     * @param data 请求实体
     * @author MrHu
     * @return String
     * @throws
     */
    public static InputStream sendPostForInputStream(String url, JSONObject data) {
        // 设置默认请求头
        Map<String, String> headers = new HashMap<>();
        headers.put("content-type", "application/json");

        return sendPostForInputStream(url, headers, data);
    }

    /**
     * @Title: sendPost
     * @Description: TODO(发送post请求，请求数据默认使用json格式，默认使用UTF-8编码)
     * @param url 请求地址
     * @param params 请求实体
     * @author MrHu
     * @return String
     * @throws
     */
    public static String sendPost(String url,Map<String,Object> params){
        // 设置默认请求头
        Map<String, String> headers = new HashMap<>();
        headers.put("content-type", "application/json");
        // 将map转成json
        JSONObject data = JSONObject.parseObject(JSON.toJSONString(params));
        return sendPost(url,headers,data,ENCODING);
    }

    /**
     * @Title: sendPost
     * @Description: TODO(发送post请求，请求数据默认使用UTF-8编码)
     * @param url 请求地址
     * @param headers 请求头
     * @param data 请求实体
     * @author MrHu
     * @return String
     * @throws
     */
    public static String sendPost(String url, Map<String, String> headers, JSONObject data) {
        return sendPost(url, headers, data, ENCODING);
    }

    /**
     * @Title: sendPost
     * @Description:(发送post请求，请求数据默认使用UTF-8编码)
     * @param url 请求地址
     * @param headers 请求头
     * @param params 请求实体
     * @author MrHu
     * @return String
     * @throws
     */
    public static String sendPost(String url,Map<String,String> headers,Map<String,String> params){
        // 将map转成json
        JSONObject data = JSONObject.parseObject(JSON.toJSONString(params));
        return sendPost(url,headers,data,ENCODING);
    }

    /**
     * @Title: sendGet
     * @Description: TODO(发送get请求)
     * @param url 请求地址
     * @param params 请求参数
     * @param encoding 编码
     * @author MrHu
     * @return String
     * @throws
     */
    public static String sendGet(String url,Map<String,Object> params,String encoding){
        // 请求结果
        String resultJson = null;
        // 创建HttpGet
        HttpGet httpGet = new HttpGet();
        httpGet.setHeader("Cookie","bid=cK_9S-Sxt98; ll=\"118267\"; __yadk_uid=wlgMy24zjGBz5xtKJSVos8LUaIQmn3Lc; push_noty_num=0; push_doumail_num=0; _vwo_uuid_v2=D93F04A0B0013A7B9C430D5A0A5FAAA15|6dd118f99734c2a91afec693aeff8aa5; dbcl2=\"170872872:2nMYz0HLkZ0\"; ck=5nOL; _pk_ref.100001.4cf6=%5B%22%22%2C%22%22%2C1553436132%2C%22https%3A%2F%2Faccounts.douban.com%2Fpassport%2Flogin%3Fredir%3Dhttps%253A%252F%252Fmovie.douban.com%252F%22%5D; _pk_id.100001.4cf6=618289941fc86136.1553363872.2.1553436132.1553363872.; _pk_ses.100001.4cf6=*; __utma=30149280.1248772063.1553363872.1553363872.1553436132.2; __utmb=30149280.0.10.1553436132; __utmc=30149280; __utmz=30149280.1553436132.2.2.utmcsr=accounts.douban.com|utmccn=(referral)|utmcmd=referral|utmcct=/passport/login; __utma=223695111.763944626.1553363872.1553363872.1553436132.2; __utmb=223695111.0.10.1553436132; __utmc=223695111; __utmz=223695111.1553436132.2.2.utmcsr=accounts.douban.com|utmccn=(referral)|utmcmd=referral|utmcct=/passport/login");
        // 创建client
        CloseableHttpClient client = HttpClients.createDefault();
        try {
             getData(url, params, httpGet);
            // 发送请求，返回响应对象
            CloseableHttpResponse response = client.execute(httpGet);
            // 获取响应状态
            int status = response.getStatusLine().getStatusCode();
            if(status == HttpStatus.SC_OK){
                // 获取响应数据
                resultJson = EntityUtils.toString(response.getEntity(), encoding);
            }else{
                System.out.println("响应失败，状态码：" + status);
            }
        } catch (Exception e) {
            System.out.println("发送get请求失败:" + e);
        } finally {
            httpGet.releaseConnection();
        }
        return resultJson;
    }

    /**
     *  get请求发起
     * @param url
     * @param params
     * @param httpGet
     * @return
     * @throws URISyntaxException
     * @throws IOException
     */
    private static void getData(String url, Map<String,Object> params, HttpGet httpGet) throws URISyntaxException, IOException {
        // 创建uri
        URIBuilder builder = new URIBuilder(url);
        // 封装参数
        if(params != null){
            for (String key : params.keySet()) {
                builder.addParameter(key, params.get(key).toString());
            }
        }
        URI uri = builder.build();
        // 设置请求地址
        httpGet.setURI(uri);

    }


    /**
     * @Title: sendGet
     * @Description: TODO(发送get请求)
     * @param url 请求地址
     * @param params 请求参数
     * @author MrHu
     * @return String
     * @throws
     */
    public static void sendGetAndSaveImg(String url, Map<String,Object> params){
        // 创建client
        CloseableHttpClient client = HttpClients.createDefault();
        // 创建HttpGet
        HttpGet httpGet = new HttpGet();
        try {
            getData(url, params, httpGet);
            // 发送请求，返回响应对象
            CloseableHttpResponse response = client.execute(httpGet);
            // 获取响应状态
            int status = response.getStatusLine().getStatusCode();
            if(status == HttpStatus.SC_OK){
                // 获取响应数据
                InputStream inputStream = response.getEntity().getContent();
                String[] urlS = url.split("/");
                FileUtil.savePic(inputStream, urlS[urlS.length-1]);
            }else{
                System.out.println("响应失败，状态码：" + status);
            }
        } catch (Exception e) {
            System.out.println("发送get请求失败:" + e);
        } finally {
            httpGet.releaseConnection();
        }
    }

    /**
     * @Title: sendGet
     * @Description: TODO(发送get请求)
     * @param url 请求地址
     * @param params 请求参数
     * @author MrHu
     * @return String
     * @throws
     */
    public static String sendGet(String url,Map<String,Object> params){
        return sendGet(url,params,ENCODING);
    }

    /**
     * @Title: sendGet
     * @Description: TODO(发送get请求)
     * @param url 请求地址
     * @author MrHu
     * @return String
     * @throws
     */
    public static void sendGetAndSaveImg(String url){
        sendGetAndSaveImg(url,null);
    }

    /**
     * @Title: sendGet
     * @Description: TODO(发送get请求)
     * @param url 请求地址
     * @author MrHu
     * @return String
     * @throws
     */
    public static String sendGet(String url){
        return sendGet(url,null,ENCODING);
    }

}
