/** ******** 登录页面 **LCL******* */
$(document).ready(function() {
	var logincount = 0;
	$("#loginButton").click(function() {
		var flag = true;
		if ($("#loginusername").val().length <= 0) {
			if ($("#loginpassword").val().length <= 0) {
				$(".message").html("请输入用户名和密码");
				flag = false;
				logincount++;
				if (logincount >= 3) {
					$("#insms").css("display", "table");
					$("#tsms").css("display", "block");
				}
			} else {
				$(".message").html("请输入用户名");
				logincount++;
				if (logincount >= 3) {
					$("#insms").css("display", "table");
					$("#tsms").css("display", "block");
				}
				flag = false;
			}
		} else {
			if ($("#loginpassword").val().length <= 0) {
				$(".message").html("请输入登录密码");
				logincount++;
				flag = false;
			}
		}
		if (flag) {
			$("#form").submit();
		}
	});
});
/** ******** 登录页面END **LH**** */

/** ******** 注册页面 **LH******* */
$(document)
		.ready(
				function() {
					$("#username").blur(
							function() {
								if ($("#username").val().length < 2
										|| $("#username").val().length > 10) {
									$(".message").html("用户名长度在2~10位之间");
								} else {
									$(".message").html("");
								}
							});
					$("#password").blur(
							function() {
								if ($("#password").val().length < 6
										|| $("#password").val().length > 18) {
									$(".message").html("密码长度在6~18位之间");
								} else {
									$(".message").html("");
								}
							});
					$("#repassword")
							.blur(
									function() {
										if ($("#repassword").val().length < 6
												|| $("#repassword").val().length > 18) {
											$(".message").html("密码长度在6~18位之间");
										} else {
											if ($("#repassword").val() != $(
													"#password").val()) {
												$(".message")
														.html("两次输入的密码不相等");
											} else {
												$(".message").html("");
											}
										}
									});
					$("#registButton")
							.click(
									function() {
										var flag = true;
										if ($("#username").val().length < 2
												|| $("#username").val().length > 10) {
											$(".message").html("用户名长度在2~10位之间");
											flag = false;
											return;
										} else {
											$(".message").html("");
										}
										if ($("#password").val().length < 6
												|| $("#password").val().length > 18) {
											$(".message").html("密码长度在6~18位之间");
											flag = false;
											return;
										} else {
											$(".message").html("");
										}
										if ($("#repassword").val().length <= 0) {
											$(".message").html("请再次输入的密码");
											flag = false;
											return;
										} else {
											if ($("#repassword").val() != $(
													"#password").val()) {
												$(".message")
														.html("两次输入的密码不相等");
												flag = false;
												return;
											} else {
												$(".message").html("");
											}
										}
										if ($("#telephone").val().length == 0) {
											$(".message").html("请输入手机号");
											flag = false;
											return;
										} else {
											if ($("#telephone").val().length < 11) {
												$(".message").html("请输入十一位手机号");
												flag = false;
												return;
											}
										}
										if (flag) {
											$("form").submit();
										}
									});
					if (document.documentElement.clientWidth < 1000) {
						$(".loginbody").css("background", "white");
						$(".loginbody").css("background", "white");
					}
				});
/** ******** 注册页面END ********* */

/** ******** 修改密码 ********* */
$(document).ready(
		function() {
			$("#passwordold").blur(function() {
				if ($("#passwordold").val().length <= 0) {
					$(".message").html("请输入原密码");
				} else {
					$(".message").html("");
				}
			});
			$("#passwordnew").blur(
					function() {
						if ($("#passwordnew").val().length < 6
								|| $("#passwordnew").val().length > 18) {
							$(".message").html("密码长度在6~18位之间");
						} else {
							$(".message").html("");
						}
					});
			$("#passwordnew2").blur(
					function() {
						if ($("#passwordnew2").val().length < 6
								|| $("#passwordnew").val().length > 18) {
							$(".message").html("密码长度在6~18位之间");
						} else {
							if ($("#passwordnew2").val() != $("#passwordnew")
									.val()) {
								$(".message").html("两次输入的新密码不相等");
							} else {
								$(".message").html("");
							}
						}
					});
			$("#passwordButton").click(
					function() {
						var flag = true;
						if ($("#passwordold").val().length <= 0) {
							$(".message").html("请输入原密码");
							flag = false;
							return;
						} else {
							$(".message").html("");
						}
						if ($("#passwordnew").val().length < 6
								|| $("#passwordnew").val().length > 18) {
							$(".message").html("密码长度在6~18位之间");
							flag = false;
							return;
						} else {
							$(".message").html("");
						}
						if ($("#passwordnew2").val().length < 6
								|| $("#passwordnew").val().length > 18) {
							$(".message").html("密码长度在6~18位之间");
							flag = false;
							return;
						} else {
							if ($("#passwordnew2").val() != $("#passwordnew")
									.val()) {
								$(".message").html("两次输入的新密码不相等");
								flag = false;
								return;
							} else {
								$(".message").html("");
							}
						}
						if (flag) {
							$("#passwordfrom").submit();

						}
					});
		});
/** ******** 修改密码END ****** */
