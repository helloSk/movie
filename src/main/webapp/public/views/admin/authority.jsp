<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>错误页面</title>
    <link rel="stylesheet" type="text/css" href="login/css/reset.css"/>
    <link rel="stylesheet" type="text/css" href="login/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="login/css/font-awesome.min.css"/>
</head>
<body>
<div class="wrap login_wrap">
    <div class="content">
        <div class="logo"></div>
        <div class="login_box">
            <div class="login_form">
                <div class="login_title">出错啦</div>
                <div class="login_title">
                    对不起，您没有访问权限！<br> <a href="javascript:;" onclick="javascript:history.back(-1);">返回上一页</a>
                </div>
                <div class="other_login"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="login/js/jquery.min.js"></script>
<script type="text/javascript" src="login/js/common.js"></script>
</body>
</html>