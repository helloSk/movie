<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	request.setAttribute("basePath",basePath);
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title>管理员首页</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body class="skin-black">
	<header class="header">
		<a href="admin/index.do" class="logo"> 电影管理系统</a>
		<nav class="navbar navbar-static-top" role="navigation">
			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="admin/password.do"><span class="glyphicon glyphicon-lock"></span> 修改密码 </a></li>
					<li><a href="logout.do"><span class="glyphicon glyphicon-log-out"></span> 注销登录 </a></li>
				</ul>
			</div>
		</nav>
	</header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<aside class="left-side sidebar-offcanvas">
			<section class="sidebar">
				<div class="user-panel">
					<div class="pull-left image">
						<img src="img/admin.png" class="img-circle" alt="User Image" />
					</div>
					<div class="pull-left info">
						<p>${user.username}</p>
						<a href="javascript:;"><i class="fa fa-circle text-success"></i> 欢迎登录</a>
					</div>
				</div>
				<ul class="sidebar-menu">
					<li>
						<a href="admin/getHot.do">
							<span class="glyphicon glyphicon-align-left"></span> 
							<span>最近热门</span>
						</a>
					</li>
					<li>
						<a href="admin/getRecentry.do"> 
							<span class="glyphicon glyphicon-align-left"></span>
							<span>即将上映</span>
						</a>
					</li>
					<li>
						<a href="admin/getUser.do">
							 <span class="glyphicon glyphicon-align-left"></span>
							 <span>用户列表</span>
						</a>
					</li>
					<li>
						<a href="admin/getApply.do">
							<span class="glyphicon glyphicon-align-left"></span>
							<span>会员申请</span>
						</a>
					</li>
					<li>
						<a href="admin/getMovieSeat.do">
							 <span class="glyphicon glyphicon-align-left"></span>
							 <span>已提交座位</span>
						</a>
					</li>
				</ul>
			</section>
		</aside>
		<aside class="right-side">
			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
							<header class="panel-heading">
								<span class="glyphicon glyphicon-text-size"></span> 系统简介
							</header>
							<div class="panel-footer bg-white">
								<div class="row">
									<div class="col-md-12">
										<h2>电影管理系统</h2>
									</div>
								</div>
								<div class="row" style="margin-top: 15px">
									<div class="col-md-12" style="color: #4c74b1">
										<p>电影管理系统，随着电影的发展，现在已经成为大多数人不可或缺的休闲娱乐方式，</p>
										<p>在身心疲惫的时候，看一个电影，是一种很好的放松方式，此系统与当前流行电影</p>
										<p>同步更新，用户可以查看当前新上映和即将上映的电影，选择自己想要的座位后</p>
										<p>购买电影票，后台管理方便，管理员无需手动上传电影名字和图片，只需点击更新</p>
										<p>即可，此系统将自动为您更新当前即将上映和当前热门的电影，解决了许多不必要</p>
										<p>的流程。</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</aside>
	</div>
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>