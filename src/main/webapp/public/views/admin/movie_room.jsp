<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title>热门电影</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body class="skin-black">
	<header class="header">
		<a href="admin/index.do" class="logo">电影管理系统 </a>
		<nav class="navbar navbar-static-top" role="navigation">
			<a href="javascript:void(0);" class="navbar-btn sidebar-toggle"
				data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle
					navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="admin/password.do"><span
							class="glyphicon glyphicon-lock"></span> 修改密码 </a></li>
					<li><a href="logout.do"><span
							class="glyphicon glyphicon-log-out"></span> 注销登录 </a></li>
				</ul>
			</div>
		</nav>
	</header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<aside class="left-side sidebar-offcanvas">
			<section class="sidebar">
				<div class="user-panel">
					<div class="pull-left image">
						<img src="img/admin.png" class="img-circle"
							alt="User Image" />
					</div>
					<div class="pull-left info">
						<p>${user.username}</p>
						<a href="javascript:;"><i class="fa fa-circle text-success"></i>
							欢迎登录</a>
					</div>
				</div>
				<ul class="sidebar-menu">
					<li>
						<a href="javascript:void(0);">
							<span class="glyphicon glyphicon-align-left"></span>
							<span>最近热门</span>
						</a>
					</li>
					<li>
						<a href="admin/getRecentry.do">
							<span class="glyphicon glyphicon-align-left"></span> 
							<span>即将上映</span>
						</a>
					</li>
					<li>
						<a href="admin/getUser.do">
							 <span class="glyphicon glyphicon-align-left"></span>
							 <span>用户列表</span>
						</a>
					</li>
					<li>
						<a href="admin/getApply.do">
							<span class="glyphicon glyphicon-align-left"></span>
							<span>会员申请</span>
						</a>
					</li>
					<li>
						<a href="admin/getMovieSeat.do">
							 <span class="glyphicon glyphicon-align-left"></span>
							 <span>已提交座位</span>
						</a>
					</li>
				</ul>
			</section>
		</aside>
		<aside class="right-side">
			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
							<header class="panel-heading">
								<span class="glyphicon glyphicon-user"></span>电影影厅
							</header>
							<div class="panel-footer bg-white"
								style="height: 560px; overflow: auto;">
								<div class="row">
									<table class="table table-bordered table-hover"
										style="table-layout: fixed;">
										<thead>
											<tr>
												<th class="col-md-1" style="text-align: center;">序号</th>
												<th class="col-md-1" style="text-align: center;">电影名字</th>
												<th class="col-md-1" style="text-align: center;">影厅名字</th>
												<th class="col-md-1" style="text-align: center;">价格</th>
												<th class="col-md-1" style="text-align: center;">座位数</th>
												<th class="col-md-1" style="text-align: center;">操作</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${rooms}" var="room" varStatus="index">
												<tr align="center">
													<td style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">${index.count}</td>
													<td style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">${room.movieName}</td>
													<td style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">${room.name}</td>
													<td style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">${room.price}</td>
													<td style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">${room.seatSize}</td>
													<td style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">
														<a href="admin/deleteRoomById.do?roomId=${room.id}&&movieName=${movieName}&&movieId=${movieId}" class="btn btn-danger btn-sm" onclick="return confirm('删除后用户将不会再看到,确定删除该影厅吗？');">删除</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
								<div class="row">
									<form class="form-horizontal" id="roomF" action="admin/addRoom.do">
										<input type="hidden" name="movieName" value="${movieName}">
										<input type="hidden" name="movieId" value="${movieId}">
										<div class="input-group input-group-sm ">
											<span class="">影厅名字</span> 
											<input type="text" class="form-control" maxlength="15" id="roomName" name="roomName" placeholder="请输入影厅名字" />
										</div>
										<div class="input-group input-group-sm ">
											<span class="">价格(元)</span>
											<input type="number" class="form-control" maxlength="15" id="price" name="price" placeholder="请输入价格" />
										</div>
										<div class="input-group input-group-sm ">
											<span class="">会员价格(元)</span>
											<input type="number" class="form-control" maxlength="15" id="mprice" name="price" placeholder="请输入会员价格" />
										</div>
										<div class="input-group input-group-sm ">
											<span class="">座位数</span>
											<input type="number" class="form-control" maxlength="3" id="seatSize" name="seatSize" placeholder="请输入座位数" />
										</div>
										<div class="input-group" style="margin-top: 10px">
											<button class="btn btn-md btn-danger" type="button" onclick="submitRoom()" >添加</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</aside>
	</div>
	<script type="text/javascript">
		function submitRoom(){
			if($("#roomName").val().length!=0){
				if($("#price").val().length!=0){
					$("#roomF").submit();
				}else{
					alert("请输入价格");
				}
			}else{
				alert("请输入影厅名字");
			}
		}
	</script>
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>