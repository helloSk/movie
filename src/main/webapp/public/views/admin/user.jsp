<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title>用户管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body class="skin-black">
	<header class="header">
		<a href="admin/index.do" class="logo">育儿论坛管理系统 </a>
		<nav class="navbar navbar-static-top" role="navigation">
			<a href="javascript:;" class="navbar-btn sidebar-toggle"
				data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle
					navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="admin/password.do"><span
							class="glyphicon glyphicon-lock"></span> 修改密码 </a></li>
					<li><a href="logout.do"><span
							class="glyphicon glyphicon-log-out"></span> 注销登录 </a></li>
				</ul>
			</div>
		</nav>
	</header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<aside class="left-side sidebar-offcanvas">
			<section class="sidebar">
				<div class="user-panel">
					<div class="pull-left image">
						<img src="img/admin.png" class="img-circle"
							alt="User Image" />
					</div>
					<div class="pull-left info">
						<p>${user.username}</p>
						<a href="javascript:;"><i class="fa fa-circle text-success"></i>
							欢迎登录</a>
					</div>
				</div>
				<ul class="sidebar-menu">
					<li>
						<a href="admin/getHot.do"> 
							<span class="glyphicon glyphicon-align-left"></span>
							<span>最近热门</span>
						</a>
					</li>
					<li>
						<a href="admin/getRecentry.do">
							 <span class="glyphicon glyphicon-align-left"></span>
							 <span>即将上映</span>
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<span class="glyphicon glyphicon-align-left"></span> 
							<span>用户列表</span>
						</a>
					</li>
					<li>
						<a href="admin/getApply.do">
							<span class="glyphicon glyphicon-align-left"></span>
							<span>会员申请</span>
						</a>
					</li>
					<li>
						<a href="admin/getMovieSeat.do">
							 <span class="glyphicon glyphicon-align-left"></span>
							 <span>已提交座位</span>
						</a>
					</li>
				</ul>
			</section>
		</aside>
		<aside class="right-side">
			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
							<header class="panel-heading">
								<span class="glyphicon glyphicon-user"></span> 用户列表
							</header>
							<div class="panel-footer bg-white"
								style="height: 560px; overflow: auto;">
								<div class="row">
									<table class="table table-bordered table-hover"
										style="table-layout: fixed;">
										<thead>
											<tr>
												<th class="col-md-1" style="text-align: center;">序号</th>
												<th class="col-md-1" style="text-align: center;">用户名</th>
												<th class="col-md-1" style="text-align: center;">密码</th>
												<th class="col-md-1" style="text-align: center;">手机号</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${users}" var="user" varStatus="index">
												<tr align="center">
													<td style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">${index.count}</td>
													<td style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">${user.username}</td>
													<td style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">${user.password}</td>
													<td style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">${user.telephone}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</aside>
	</div>
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>