<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="base.jsp"%>
<title>首页</title>
<style type="text/css">
	.marginButtom{
		margin-bottom: 14px;
	}


</style>
</head>
<body>
	<div class="header">
		<div class="content white agile-info">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<h1 style="letter-spacing: 1px;font-weight: 600;text-transform: uppercase;color: #282828">电影小助手 </h1>
					</div>
					<!--/.navbar-header-->
					<div class="collapse navbar-collapse">
						<nav class="link-effect-2">
							<ul class="nav navbar-nav">
								<c:if test="${user.username == null}">
									<li><a href="user/toLogin.do" class="effect-3">登录/注册</a></li>
								</c:if>
								<c:if test="${user.username != null}">
									<li class="dropdown">
										<a href="javascript:return false;" class="dropdown-toggle effect-3" data-toggle="dropdown" aria-expanded="false">${user.username}
											<b class="caret"></b>
										</a>
										<ul class="dropdown-menu">
											<li>
												<a href="user/toMyMovieSeat.do?userId=${user.id}">已选座位</a>
											</li>
											<li>
												<c:choose>
													<c:when test="${user.admin==4}">
														<a href="javascript:return false;">已是会员</a>
													</c:when>
													<c:otherwise>
														<c:if test="${user.admin==3}">
															<a href="javascript:return false;">已申请</a>
														</c:if>
														<c:if test="${user.admin!=3}">
															<a href="user/applyMember.do">申请会员</a>
														</c:if>
													</c:otherwise>
												</c:choose>
											</li>
											<li data-toggle="modal" data-target="#myModal">
												<a>修改密码</a>
											</li>
										</ul>
									</li>
									<li><a class="dropdown-toggle effect-3" href="user/rank.do">电影排名</a></li>
									<li><a class="dropdown-toggle effect-3" href="logout.do">注销</a></li>
								</c:if>
							</ul>
						</nav>
					</div>
				</div>
			</nav>
		</div>
	</div>
	<div class="services-breadcrumb_w3layouts">
		<div class="inner_breadcrumb">
			<ul class="short_w3ls">
				<li><a href="javascript:return false;">首页</a><span>|</span></li>
			</ul>
		</div>
	</div>
	<div class="container">
		<div class="inner_sec_grids_info_w3ls"></div>
			<div class="col-md-8">
				<div class="but_list">
					<div class="bs-example bs-example-tabs" role="tabpanel">
						<ul id="myTab" class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#home" id="" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">电影分享</a></li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
								<div class="tab_grid">
									<div class="col-sm-3 loc_1">
										<img src="img/shetuantupian.jpg" alt="正在加载">
									</div>
									<div class="col-sm-9">
										<div class="location_box1">
											<h6>
												<a href="javascript:return false;">网站介绍 </a>
												<span style="margin-left: 30px"> 搜索电影：</span> <input type="text" id="movieName" name="movieName" value="${movieName}" placeholder="请输入电影名称">
											</h6>
											<p>
												<span class="m_2">简介 : </span>电影管理系统，随着电影飞速的发展，现在已经成为大多数人不可或缺的休闲娱乐方式，在心情郁闷的时候，有些人会选择运动一下，但是更多的人会选择看一场电影，因为是一种非常方便而且容易实现的一种放松方式，此网站与当前流行电影同步更新，用户可以第一时间查看当前新上映和即将上映的电影，选择自己想要的座位后购买电影票，方便快捷。
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<c:if test="${hotMovies.size()>0}">
					<c:forEach items="${hotMovies}"  begin="0" end="1" var="hotMovie" varStatus="index">
						<div class="col-md-4">
							<div><img width="200px" onclick="toDetail(${hotMovie.id})" style="cursor: pointer" height="300px" alt="正在加载" src="${hotMovie.cover}"></div>
							<div>
								<span style="color: #ffb900">
									<c:choose>
										<c:when test="${hotMovie.title.length()>8}">
											${hotMovie.title.substring(0,8)}...
										</c:when>
										<c:otherwise>
											${hotMovie.title}
										</c:otherwise>
									</c:choose>
								</span> <span style="color: #e7494f;padding-left: 20px">${hotMovie.rate}</span></div>
							<div>
								<%--<span style="color: red;margin-right: 10px">${hotMovie.price}元</span>--%>
								<a class="btn btn-md btn-info" href="user/toSeat.do?movieId=${hotMovie.id}">选座</a>
								<a class="btn btn-md btn-danger" href="user/toLeaveMessage.do?movieId=${hotMovie.id}">评价</a>
							</div>
						</div>
					</c:forEach>
				</c:if>
			</div>
		<div class="col-md-4">
			<div class="col_3 permit">
				<h3>即将上映</h3>
				<ul class="list_2">
					<c:if test="${recentryMovies.size()>0}">
						<c:forEach items="${recentryMovies}" begin="0" end="17" var="recentryMovie" varStatus="index">
							<li style="text-align: center">
								<span style="color: #ffb900;">
									<c:if test="${recentryMovie.title.length()>10}">
										${recentryMovie.title.substring(0,10)}...
									</c:if>
									<c:if test="${recentryMovie.title.length()<11}">
										${recentryMovie.title}
									</c:if>
								</span>
								<span style="padding-left: 30px;color: #e7494f">
									${recentryMovie.rate}
								</span>
							</li>
						</c:forEach>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
	<div class="container" style="margin-bottom: 40px">
		<c:if test="${hotMovies.size()>0}">
			<c:forEach items="${hotMovies}" begin="2"  var="hotMovie" varStatus="index">
				<div class="col-md-3" style="margin-top: 20px">
					<div><img onclick="toDetail(${hotMovie.id})" style="cursor: pointer" width="220px" height="350px" alt="正在加载" src="${hotMovie.cover}"></div>
					<div>
						<span style="color: #ffb900">
							<c:choose>
								<c:when test="${hotMovie.title.length()>8}">
									${hotMovie.title.substring(0,8)}...
								</c:when>
								<c:otherwise>
									${hotMovie.title}
								</c:otherwise>
							</c:choose>
						</span>
						<span style="color: #e7494f;padding-left: 20px">${hotMovie.rate}</span></div>
					<div>
						<%--<span style="color: red;margin-right: 10px">${hotMovie.price}元</span>--%>
						<a class="btn btn-md btn-info" href="user/toSeat.do?movieId=${hotMovie.id}">选座</a>
						<a class="btn btn-md btn-danger" href="user/toLeaveMessage.do?movieId=${hotMovie.id}">评价</a>
					</div>
				</div>
			</c:forEach>
		</c:if>
	</div>
	<div class="modal fade" id="myModal" style="margin-top: 9%" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">
						修改密码
					</h4>
				</div>
				<div class="modal-body">
					<form id="updateP" action="user/updatePassword">
						<div class="marginButtom">
							<label for="password">密码</label>
							<input type="password" class="form-control" name="password" id="password" placeholder="请输入密码">
						</div>
						<div class="marginButtom">
							<label for="newPassword">新密码</label>
							<input type="password" class="form-control" name="newPassword" id="newPassword" placeholder="请输入新密码">
						</div>
						<div class="marginButtom">
							<label for="newPassword2">确认密码</label>
							<input type="password" class="form-control" name="newPassword2" id="newPassword2" placeholder="请输入确认密码">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
					<button type="button" id="updatePassword" class="btn btn-primary">
						提交更改
					</button>
				</div>

			</div>
		</div>
	</div>
<input type="hidden" id="myPassword" value="${user.password}">
</body>
<script type="application/javascript" >
	function toDetail(movieId) {
		window.location.href="user/toMovieDetail?movieId="+movieId+"";
	}
	$(function () {
		$("body").keydown(function (event) {
			if (event.keyCode == "13") { //keyCode=13是回车键
				var movieName = $("#movieName").val();
				window.location.href="user/toHome?movieName="+movieName+""
			}
		});
	});
	$("#updatePassword").on("click",function () {
		if ($("#password").val() == $("#myPassword").val()){
			if ($("#newPassword").val() == $("#newPassword2").val()){
				$("#updateP").submit();
			}else{
				alert("两次密码输入不一致")
			}
		}else{
			alert("密码输入错误");
		};
	});

</script>
</html>