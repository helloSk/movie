<%@ page contentType="text/html;charset=UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    request.setAttribute("basePath",basePath);
%>
<html>
<head>
<title>用户登录</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            background: #fff;
            color: #fff;
            font-family: Arial;
            font-size: 12px;
        }
        html { overflow-x: hidden; overflow-y: hidden; }
        .body {
            position: absolute;
            top: -20px;
            left: -20px;
            right: -40px;
            bottom: -40px;
            width: auto;
            height: auto;
            background-image: url(http://ginva.com/wp-content/uploads/2012/07/city-skyline-wallpapers-008.jpg);
            background-size: cover;
            -webkit-filter: blur(5px);
            z-index: 0;
        }

        .grad {
            position: absolute;
            top: -20px;
            left: -20px;
            right: -40px;
            bottom: -40px;
            width: auto;
            height: auto;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(0, 0, 0, 0)), color-stop(100%, rgba(0, 0, 0, 0.65))); /* Chrome,Safari4+ */
            z-index: 1;
            opacity: 0.7;
        }

        .header {
            position: absolute;
            top: calc(50% - 35px);
            left: calc(50% - 255px);
            z-index: 2;
        }

        .header div {
            float: left;
            color: #fff;
            font-family: 'Exo', sans-serif;
            font-size: 35px;
            font-weight: 200;
        }

        .header div span {
            color: #5379fa !important;
        }

        .login {
            position: absolute;
            top: calc(50% - 100px);
            left: calc(50% - 50px);
            height: 150px;
            width: 350px;
            padding: 10px;
            z-index: 2;
        }

        .login input[type=text] {
            width: 260px;
            height: 35px;
            background: transparent;
            border: 1px solid rgba(255, 255, 255, 0.6);
            border-radius: 2px;
            color: #fff;
            font-family: 'Exo', sans-serif;
            font-size: 16px;
            font-weight: 400;
            padding: 4px;
        }

        .login input[type=password] {
            width: 260px;
            height: 35px;
            background: transparent;
            border: 1px solid rgba(255, 255, 255, 0.6);
            border-radius: 2px;
            color: #fff;
            font-family: 'Exo', sans-serif;
            font-size: 16px;
            font-weight: 400;
            padding: 4px;
            margin-top: 10px;
        }

        .login input[type=button] {
            width: 260px;
            height: 35px;
            background: #fff;
            border: 1px solid #fff;
            cursor: pointer;
            border-radius: 2px;
            color: #a18d6c;
            font-family: 'Exo', sans-serif;
            font-size: 16px;
            font-weight: 400;
            padding: 6px;
            margin-top: 10px;
        }

        .login input[type=button]:hover {
            opacity: 0.8;
        }

        .login input[type=button]:active {
            opacity: 0.6;
        }

        .login input[type=text]:focus {
            outline: none;
            border: 1px solid rgba(255, 255, 255, 0.9);
        }

        .login input[type=password]:focus {
            outline: none;
            border: 1px solid rgba(255, 255, 255, 0.9);
        }

        .login input[type=button]:focus {
            outline: none;
        }

        ::-webkit-input-placeholder {
            color: rgba(255, 255, 255, 0.6);
        }

        input:-webkit-autofill , textarea:-webkit-autofill, select:-webkit-autofill {
            -webkit-text-fill-color: #ededed !important;
            -webkit-box-shadow: 0 0 0px 1000px transparent  inset !important;
            transition: background-color 50000s ease-in-out 0s;
        }
        input {
            background-color:transparent;
        }
        .message{
            margin-bottom: 14px;
            font-size: 15px;
            color: #4f453e;
            height: 15px;
        }

    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
</head>
<body>
    <div class="body"></div>
    <div class="grad"></div>
    <div class="header">
        <div>Love<span>&nbsp;Movies</span></div>
    </div>
    <br>
    <div class="login">
        <form method="post" id="userLogin" action="${basePath}login">
            <div id="error" class="message">${message}</div>
            <input type="text" id="user" placeholder="请输入账号" name="username"><br>
            <input type="password" id="password" placeholder="请输入密码" name="password"><br>
            <input type="hidden" name="type" value="0">
            <div style="margin-top: 10px"><span onclick="toRegist()" style="cursor: pointer">没有账号？</span></div>
            <input id="log" type="button" value="登录">
        </form>
        <form method="post" style="display: none" id="userRigist" action="${basePath}regist">
            <div class="message">${message}</div>
            <input type="text" id="user2" placeholder="请输入账号" name="username"><br>
            <input type="password" id="password2" placeholder="请输入密码" name="password"><br>
            <input type="text" placeholder="请输入电话号码" name="telephone" style="margin-top: 10px"><br>
            <input type="hidden" name="isAdmin" value="0">
            <input id="regist" type="button" value="注册">
        </form>
    </div>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function toRegist() {
            $("#userLogin").hide();
            $("#userRigist").show();
        };
        $(function () {
            // $('#user').focus();
            $('#user,#password').on('input propertychange', addError).blur(addError) //input 输入 失去焦点时触发
            $('#log').click(loginTest);
            $('#regist').click(loginTest2);
            $("body").keydown(function (event) {
                if (event.keyCode == "13") { //keyCode=13是回车键
                    loginTest();
                }
            });

        });

        //登录判断
        function loginTest() {
            if (loginAddError()){
                $("#userLogin").submit();
            }
        }

        //注册判断
        function loginTest2() {
            if (loginAddError2()){
                $("#userRigist").submit();
            }
        }

        //输入框判断  空与16长度
        function addError() {
            if ($(this).val() == '') {
                $('#error').html('用户名不能为空')
                if ($(this).prop('id') == 'password') {
                    $('#error').html('密码不能为空')
                }
            } else if ($(this).val().length >= 16) {
                $('#error').html('用户名长度不能超过16')
                if ($(this).prop('id') == 'password') {
                    $('#error').html('密码长度不能超过16')
                }
            } else {
                $("#error").html("");
            }
        }

        //默认不输入  直接登录判断
        function loginAddError() {
            if ($('#user').val() == '') {
                $('#user').focus().siblings('#error').html('用户名不能为空')
                return false;
            } else if ($('#password').val() == '') {
                $('#password').focus().siblings('#error').html('密码不能为空')
                return false;
            }
            return true;
        }

        //默认不输入  直接登录判断
        function loginAddError2() {
            if ($('#user2').val() == '') {
                $('#user2').focus().siblings('#error').html('用户名不能为空')
                return false;
            } else if ($('#password2').val() == '') {
                $('#password2').focus().siblings('#error').html('密码不能为空')
                return false;
            }
            return true;
        }

    </script>
</body>
</html>
