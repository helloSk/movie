<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

	request.setAttribute("basePath",basePath);
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<title>留言</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
<style>
	.divpadding {
		padding-top: 5px;
		padding-bottom: 5px;
		padding-left: 5px;
	}

	.divmargin {
		margin-bottom: 10px
	}

	.lineheight {
		background-color: #f1f2f7;
		height: 1px
	}

	.texts {
		color: #17A7C9;
		font-size: 16px;
	}

	.glyphicon-envelope {
		color: #d9534f;
		font-size: 16px;
	}

	body {
		background: url(img/37r8273-02.jpg);
		background-repeat: no-repeat;
		background-size: 100%;
		background-attachment: fixed;
	}
</style>
</head>
<body class="">
	<div class="container">
		<div class="row" style="padding-top: 23%; padding-bottom: 10%;">
			<div class="col-md-1"></div>
			<div class="col-md-10" style="background-color: white">
				<div style="margin-top: 15px; font-size: 16px">
					欢迎您：<span style="font-weight: bold;">${user.username}</span> <span style="color: red; font-weight: bold; font-size: 17px;">${message}</span>
				</div>
				<h3 style="color: #498B31;">${hotmovie.title}&nbsp;&nbsp;留言</h3>
				<h4 style="color: red;" id="message"></h4>
				<form id="leavemegF" action="user/postMessage.do" method="post">
					<div class="divmargin">
						<textarea name="message" id="text" class="form-control" rows="5"></textarea>
						<input type="hidden" name="userId" value="${user.id}">
						<input type="hidden" name="movieId" value="${movieId}">
						<input type="hidden" name="userName" value="${user.username}">
					</div>
					<div class="divmargin">
						<button type="button" id="submitF" class="btn btn-danger btn-sm">
							<span class="glyphicon glyphicon-saved"></span> 提交留言
						</button>
					</div>
				</form>
				<div class="divpadding" style="margin-bottom: 15px">
					<a href="user/toHome" class="btn btn-info"><span class="glyphicon glyphicon-log-out"></span> 首页 </a>
					<%--<button class="btn btn-info" id="passwordbtn">--%>
						<%--<span class="glyphicon glyphicon-lock"></span> 修改密码--%>
					<%--</button>--%>
					<a href="logout.do" class="btn btn-info"><span class="glyphicon glyphicon-log-out"></span> 注销登录 </a>
				</div>
				<div class="" id="leave">
					<c:forEach items="${leaveMessages}" var="leavemessage" varStatus="index">
						<div style="border: 1px solid #e2d84a; margin-bottom: 20px">
							<div class="divpadding">
								<i class="glyphicon glyphicon-user texts">游客：</i>
								${leavemessage.userName}
							</div>
							<div class="lineheight"></div>
							<div class="divpadding">
								<i class="glyphicon glyphicon-envelope">留言：</i>
								${leavemessage.message}
							</div>
							<div class="lineheight"></div>
						</div>
					</c:forEach>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$("#submitF").on("click",function() {
			if ($("#text").val() == null || $("#text").val() == "") {
				$("#message").html("请输入留言");
			} else {
				$("#leavemegF").submit()
			}
		});
		$("#history").on("click",function () {
			window.location.href="user/toHome";
		});
	</script>
</body>
</html>