<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="base.jsp"%>
<title>详情</title>
</head>
<style type="text/css">
    .marginTop{
        margin-top: 6px;
    }
    .blackColor{
        color: #666666;
    }
    .moreBlackColor{
        color: #111;
    }
    .smallTitle{
        margin-top: -44px;
        margin-bottom: 10px;
        color: #007722;
        font-size: 17px;
    }

</style>
<body>
<div class="header">
    <div class="content white agile-info">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <h1 style="letter-spacing: 1px; font-weight: 600; text-transform: uppercase; color: #282828">电影小助手</h1>
                </div>
                <!--/.navbar-header-->
                <div class="collapse navbar-collapse">
                    <nav class="link-effect-2">
                        <ul class="nav navbar-nav">
                            <c:if test="${user.username == null}">
                                <li><a href="user/toLogin.do" class="effect-3">登录/注册</a></li>
                            </c:if>
                            <c:if test="${user.username != null}">
                                <li class="dropdown">
                                    <a href="javascript:return false;" class="dropdown-toggle effect-3" data-toggle="dropdown" aria-expanded="false">${user.username}
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="user/toMyMovieSeat.do?userId=${user.id}">已选座位</a>
                                        </li>
                                        <li>
                                            <c:choose>
                                                <c:when test="${user.admin==4}">
                                                    <a href="javascript:return false;">已是会员</a>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:if test="${user.admin==3}">
                                                        <a href="javascript:return false;">已申请</a>
                                                    </c:if>
                                                    <c:if test="${user.admin!=3}">
                                                        <a href="user/applyMember.do">申请会员</a>
                                                    </c:if>
                                                </c:otherwise>
                                            </c:choose>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="dropdown-toggle effect-3" href="user/rank.do">电影排名</a></li>
                                <li><a class="dropdown-toggle effect-3" href="user/logout.do">注销</a></li>
                            </c:if>
                        </ul>
                    </nav>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="services-breadcrumb_w3layouts">
    <div class="inner_breadcrumb">
        <ul class="short_w3ls">
            <li><a href="user/toHome" style="color: black">首页</a><span>|</span></li>
            <li><a href="user/toSeat.do?movieId=${hotMovie.id}" style="color: black">电影选座</a><span>|</span></li>
            <li><a href="javascript:return false;">电影详情</a></li>
        </ul>
    </div>
</div>
<div class="container" style="margin-bottom: 20px">
    <div class="inner_sec_grids_info_w3ls"></div>
    <div class="row">
        <div class="col-md-8">
            <div class="but_list">
                <div class="bs-example bs-example-tabs" role="tabpanel">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="javascript:return false;" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">${hotMovie.title}详情</a></li>
                    </ul>
                    <div id="" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active"
                             aria-labelledby="home-tab">
                            <div class="tab_grid">
                                <div class="col-sm-3 loc_1">
                                    <img src="${hotMovie.cover}" alt="正在加载">
                                </div>
                                <div class="col-sm-9">
                                    <div class="location_box1">
                                        <h6>
                                            <a href="javascript:return false;">${hotMovie.title} </a>
                                        </h6>
                                        <div class="marginTop"><span class="blackColor">导演：</span><span class="moreBlackColor">${movieDetail.director}</span></div>
                                        <div class="marginTop"><span class="blackColor">编辑：</span><span class="moreBlackColor">${movieDetail.writer}</span></div>
                                        <div class="marginTop">
                                            <span class="blackColor">演员：</span>
                                            <span class="moreBlackColor">
                                                <c:choose>
                                                    <c:when test="${movieDetail.star.length()>80}">
                                                        ${movieDetail.star.substring(0,80)}...
                                                    </c:when>
                                                    <c:otherwise>
                                                        ${movieDetail.star}
                                                    </c:otherwise>
                                                </c:choose>
                                            </span>
                                        </div>
                                        <div class="marginTop"><span class="blackColor">类型：</span><span class="moreBlackColor">${movieDetail.type}</span></div>
                                        <div class="marginTop"><span class="blackColor">上映时间：</span><span class="moreBlackColor">${movieDetail.releaseTime}</span></div>
                                        <div class="marginTop"><span class="blackColor">电影时长：</span><span class="moreBlackColor">${movieDetail.movieTime}</span></div>
                                        <div class="marginTop"><span class="blackColor">IMDb链接:</span><a href="http://www.imdb.com/title/${movieDetail.imdb}" target="_blank" class="moreBlackColor" style="color: #00b9ff">&nbsp;${movieDetail.imdb}</a></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="tab_grid">
                                <div class="form-group">
                                    <div class="smallTitle">剧情介绍</div>
                                    <div>${movieDetail.plot}</div>
                                </div>
                            </div>
                            <div class="tab_grid">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="smallTitle" style="margin-left: 13px">主要演员</div>
                                    </div>
                                    <div class="row">
                                        <c:forEach items="${moviePerformers}" var="moviePerformer" varStatus="index">
                                            <div class="col-md-4">
                                                <img src="${moviePerformer.img}" width="100%" height="320px">
                                                <div class="moreBlackColor" align="center">
                                                    <c:choose>
                                                        <c:when test="${moviePerformer.movieDuties.length()>13}">
                                                            ${moviePerformer.movieDuties.substring(0,13)}...
                                                        </c:when>
                                                        <c:otherwise>
                                                            ${moviePerformer.movieDuties}
                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                                <div class="blackColor" align="center">${moviePerformer.name}</div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col_3 permit">
                <h3>即将上映</h3>
                <ul class="list_2">
                    <c:forEach items="${recentryMovies}" var="recentryMovie" varStatus="index">
                        <li style="text-align: center">
								<span style="color: #ffb900;">
									<c:if test="${recentryMovie.title.length()>10}">
                                        ${recentryMovie.title.substring(0,10)}...
                                    </c:if>
									<c:if test="${recentryMovie.title.length()<11}">
                                        ${recentryMovie.title}
                                    </c:if>
								</span>
                            <span style="padding-left: 30px; color: #e7494f">
                                    ${recentryMovie.rate}
                            </span>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">

</script>
</html>