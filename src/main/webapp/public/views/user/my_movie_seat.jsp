<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="base.jsp"%>
<title>已选座位</title>
</head>
<body>
	<div class="header">
		<div class="content white agile-info">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<h1 style="letter-spacing: 1px;font-weight: 600;text-transform: uppercase;color: #282828">电影小助手 </h1>
					</div>
					<!--/.navbar-header-->
					<div class="collapse navbar-collapse">
						<nav class="link-effect-2" id="">
							<ul class="nav navbar-nav">
								<c:if test="${user.username == null}">
									<li><a href="user/toLogin.do" class="effect-3">登录/注册</a></li>
								</c:if>
								<c:if test="${user.username != null}">
									<li class="dropdown">
										<a href="javascript:return false;" class="dropdown-toggle effect-3" data-toggle="dropdown" aria-expanded="false">${user.username}
											<b class="caret"></b>
										</a>
										<ul class="dropdown-menu">
											<li>
												<a href="javascript:return false;">已选座位</a>
											</li>
											<li>
												<c:choose>
													<c:when test="${user.admin==4}">
                                                        <a href="javascript:return false;">已是会员</a>
													</c:when>
													<c:otherwise>
														<c:if test="${user.admin==3}">
															<a href="javascript:return false;">已申请</a>
														</c:if>
														<c:if test="${user.admin!=3}">
															<a href="user/applyMember.do">申请会员</a>
														</c:if>
													</c:otherwise>
												</c:choose>
											</li>
										</ul>
									</li>
									<li><a class="dropdown-toggle effect-3" href="user/rank.do">电影排名</a></li>
									<li><a class="dropdown-toggle effect-3" href="user/logout.do">注销</a></li>
								</c:if>
							</ul>
						</nav>
					</div>
				</div>
			</nav>
		</div>
	</div>
	<div class="services-breadcrumb_w3layouts">
		<div class="inner_breadcrumb">
			<ul class="short_w3ls">
				<li><a href="user/toHome" style="color: black">首页</a><span>|</span></li>
				<li><a href="javascript:return false;">已选座位</a></li>
			</ul>
		</div>
	</div>
	<div class="container-fluid" style="margin-top: 50px">
		<div class="row">
			<div class="col-md-2 col-sm-1 hidden-xs"></div>
			<div class="col-md-8 col-sm-10 col-xs-12" id="colum">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>序号</th>
							<th>电影</th>
							<th>申请人</th>
							<th>联系电话</th>
							<th>影厅名字</th>
							<th>价格</th>
							<th>座位</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${movieSeats}" var="movieSeat" varStatus="index">
							<tr class="warning">
								<td>${index.count}</td>
								<td>${movieSeat.movieName}</td>
								<td>${user.username}</td>
								<td>${movieSeat.userTelephone}</td>
								<td>${movieSeat.roomName}</td>
								<td>${movieSeat.price}</td>
								<td>${movieSeat.number}</td>
								<td>
									<c:choose>
										<c:when test="${movieSeat.state==0}">
											<a class="btn btn-md btn-danger" href="user/deleteSeat.do?seatId=${movieSeat.id}">退座</a>
											<a class="btn btn-md btn-danger" href="user/sendOrder.do?seatId=${movieSeat.id}">提交</a>
										</c:when>
										<c:otherwise>
											<a class="btn btn-md btn btn-success" href="javascript:return false;">已提交</a>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</body>
</html>	