<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="base.jsp"%>
<title>电影评分排名</title>
</head>
<body>
<div class="header">
    <div class="content white agile-info">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <h1 style="letter-spacing: 1px;font-weight: 600;text-transform: uppercase;color: #282828">电影小助手 </h1>
                </div>
                <!--/.navbar-header-->
                <div class="collapse navbar-collapse">
                    <nav class="link-effect-2" id="">
                        <ul class="nav navbar-nav">
                            <c:if test="${user.username == null}">
                                <li><a href="user/toLogin.do" class="effect-3">登录/注册</a></li>
                            </c:if>
                            <c:if test="${user.username != null}">
                                <li class="dropdown">
                                    <a href="javascript:return false;" class="dropdown-toggle effect-3" data-toggle="dropdown" aria-expanded="false">${user.username}
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="javascript:return false;">已选座位</a>
                                        </li>
                                        <li>
                                            <c:choose>
                                                <c:when test="${user.admin==4}">
                                                    <a href="javascript:return false;">已是会员</a>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:if test="${user.admin==3}">
                                                        <a href="javascript:return false;">已申请</a>
                                                    </c:if>
                                                    <c:if test="${user.admin!=3}">
                                                        <a href="user/applyMember.do">申请会员</a>
                                                    </c:if>
                                                </c:otherwise>
                                            </c:choose>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="dropdown-toggle effect-3" href="user/rank.do">电影排名</a></li>
                                <li><a class="dropdown-toggle effect-3" href="user/logout.do">注销</a></li>
                            </c:if>
                        </ul>
                    </nav>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="services-breadcrumb_w3layouts">
    <div class="inner_breadcrumb">
        <ul class="short_w3ls">
            <li><a href="user/toHome" style="color: black">首页</a><span>|</span></li>
            <li><a href="javascript:return false;">电影排名</a></li>
        </ul>
    </div>
</div>
<div class="container-fluid" style="margin-top: 50px">
    <div class="row">
        <div class="col-md-1 col-sm-1 hidden-xs"></div>
        <div class="col-md-10 col-sm-10 col-xs-12" id="colum">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>序号</th>
                    <th>电影名</th>
                    <th>评分</th>
                    <th>价格</th>
                    <th>导演</th>
                    <%--<th>编剧</th>--%>
                    <%--<th>明星</th>--%>
                    <th>电影类型</th>
                    <th>发布时间</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${hotMovies}" var="hotMovie" varStatus="index">
                    <tr class="warning">
                        <td>${index.count}</td>
                        <td>${hotMovie.title}</td>
                        <td>${hotMovie.rate}</td>
                        <td>${hotMovie.price}元</td>
                        <td>${hotMovie.director}</td>
                        <%--<td>${hotMovie.writer}</td>--%>
                        <%--<td>${hotMovie.star}</td>--%>
                        <td>${hotMovie.type}</td>
                        <td>${hotMovie.release_time}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>