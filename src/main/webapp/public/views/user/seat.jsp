<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="base.jsp" %>
<title>选座</title>
</head>
<style type="text/css">
    .car {

    }

    .car .seat {
        height: 40px;
        width: 40px;
        background-size: 100%;
        text-align: center;
        font-size: 0;
    }

    .car .sold {
        background-image: url(img/seat_sold.png);
        background-repeat: no-repeat;
        cursor: default;
    }

    .car .enabled {
        background-image: url(img/seat_enabled.png);
        background-repeat: no-repeat;
        cursor: pointer;
    }

    .car .disable {
        background-image: url(img/seat_disable.png);
        background-repeat: no-repeat;
        cursor: default;
    }

    .car .select {
        background-image: url(img/seat_select.png);
        background-repeat: no-repeat;
        cursor: pointer;
    }
</style>
<body>
<div class="header">
    <div class="content white agile-info">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <h1
                            style="letter-spacing: 1px; font-weight: 600; text-transform: uppercase; color: #282828">
                        电影小助手
                    </h1>
                </div>
                <!--/.navbar-header-->
                <div class="collapse navbar-collapse">
                    <nav class="link-effect-2">
                        <ul class="nav navbar-nav">
                            <c:if test="${user.username == null}">
                                <li><a href="user/toLogin.do" class="effect-3">登录/注册</a></li>
                            </c:if>
                            <c:if test="${user.username != null}">
                                <li class="dropdown">
                                    <a href="javascript:return false;" class="dropdown-toggle effect-3"
                                       data-toggle="dropdown" aria-expanded="false">${user.username}
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="user/toMyMovieSeat.do?userId=${user.id}">已选座位</a>
                                        </li>
                                        <li>
                                            <c:choose>
                                                <c:when test="${user.admin==4}">
                                                    <a href="javascript:return false;">已是会员</a>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:if test="${user.admin==3}">
                                                        <a href="javascript:return false;">已申请</a>
                                                    </c:if>
                                                    <c:if test="${user.admin!=3}">
                                                        <a href="user/applyMember.do">申请会员</a>
                                                    </c:if>
                                                </c:otherwise>
                                            </c:choose>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="dropdown-toggle effect-3" href="user/rank.do">电影排名</a></li>
                                <li><a class="dropdown-toggle effect-3" href="user/logout.do">注销</a></li>
                            </c:if>
                        </ul>
                    </nav>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="services-breadcrumb_w3layouts">
    <div class="inner_breadcrumb">
        <ul class="short_w3ls">
            <li><a href="user/toHome" style="color: black">首页</a><span>|</span></li>
            <li><a href="javascript:return false;">电影选座</a></li>
        </ul>
    </div>
</div>
<div class="container" style="margin-bottom: 20px">
    <div class="inner_sec_grids_info_w3ls"></div>
    <div class="row">
        <div class="col-md-8">
            <div class="but_list">
                <div class="bs-example bs-example-tabs" role="tabpanel">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="" role="tab"
                                                                  data-toggle="tab" aria-controls="home"
                                                                  aria-expanded="true">电影选座</a></li>
                    </ul>
                    <div id="" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active"
                             aria-labelledby="home-tab">
                            <div class="tab_grid">
                                <div class="col-sm-3 loc_1">
                                    <img src="${hotMovie.cover}" alt="正在加载">
                                </div>
                                <div class="col-sm-9">
                                    <div class="location_box1">
                                        <h6>
                                            <a href="javascript:return false;">电影选座 </a>
                                        </h6>
                                        <p>
                                            此选座功能仅代表选择作为，不代表已经购买，选座成功后将有管理员购买电影票后由短信的形式进行通知，后期将完善此系统，做到直接能够购买电影票，提升用户体验，您的支持是我们最大的动力，给您带来的不便，敬请谅解。
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="tab_grid">
                                <div class="form-group">
                                    <form action="user/toSeat.do" method="post" id="roomF">
                                        <label for="roomId" style="color: #2194D6">影厅 &nbsp;&nbsp;价格: </label>${price}
                                        <select id="roomId" name="roomId" class="form-control">
                                            <c:forEach items="${rooms}" var="room" varStatus="index">
                                                <c:choose>
                                                    <c:when test="${roomId==room.id}">
                                                        <option selected="selected"
                                                                value="${room.id}">${room.name}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${room.id}">${room.name}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                        <input type="hidden" name="price" value="${price}">
                                        <input type="hidden" name="movieId" value="${hotMovie.id}">
                                    </form>
                                </div>
                                <div class="car" id="car">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="col_3 permit">
                <h3>即将上映</h3>
                <ul class="list_2">
                    <c:forEach items="${recentryMovies}" var="recentryMovie" varStatus="index">
                        <li style="text-align: center">
								<span style="color: #ffb900;">
									<c:if test="${recentryMovie.title.length()>10}">
                                        ${recentryMovie.title.substring(0,10)}...
                                    </c:if>
									<c:if test="${recentryMovie.title.length()<11}">
                                        ${recentryMovie.title}
                                    </c:if>
								</span>
                            <span style="padding-left: 30px; color: #e7494f">
                                    ${recentryMovie.rate}
                            </span>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</div>
<form action="user/addSeat.do" id="selectSeat">
    <input type="hidden" name="userTelephone" value="${user.telephone}">
    <input type="hidden" id="selectNumber" name="number" value="">
    <input type="hidden" name="movieId" value="${hotMovie.id}">
    <input type="hidden" name="movieName" value="${hotMovie.title}">
    <input type="hidden" name="userId" value="${user.id}">
    <input type="hidden" name="userName" value="${user.username}">
    <input type="hidden" name="roomName" value="${roomName}">
    <input type="hidden" name="roomId" value="${roomId}">
    <input type="hidden" name="price" value="${price}">
</form>
</body>
    <script type="text/javascript">

        window.onload = function() {
            onLoad();
        };

        function onLoad() {
            var seatSize = parseInt(${room.seatSize});
            var _html = "";
            for (var i = 0; i < seatSize; i++) {
                _html += '<div class="seat col-md-1" id="s'+i+'" onclick="select(\'s'+i+'\')" style="background-image: url(img/seat_sold.png); margin-right: 10px; margin-bottom: 10px"></div>';
            }
            $("#car").empty();
            $("#car").html(_html);

            var array = new Array();
            <c:forEach items="${movieSeats}" var="item" varStatus="status" >
            array.push("${item.number}");
            </c:forEach>
            for (var j = 0; j < array.length; j++) {
                var id = "#" + array[j];
                $(id).css("background-image", "url(img/seat_select.png)");
                $(id).attr("onclick", "");
            }
        }
        
        function select(id) {
            $("#selectNumber").val(id);
            // $("#selectSeat").submit();
            var params = {};
            var allParam = $("#selectSeat").find("input");
            params.userTelephone = $(allParam[0]).val();
            params.number = $(allParam[1]).val();
            params.movieId = $(allParam[2]).val();
            params.movieName = $(allParam[3]).val();
            params.userId = $(allParam[4]).val();
            params.userName = $(allParam[5]).val();
            params.roomName = $(allParam[6]).val();
            params.roomId = $(allParam[7]).val();
            params.price = $(allParam[8]).val();
            postJson(params, "user/addSeat.do", function (data) {
                for (var i = 0; i < data.length; i++){
                    var id = "#" + data[i].number;
                    $(id).css("background-image", "url(img/seat_select.png)");
                    $(id).attr("onclick", "");
                }
            });
        }
        $("#roomId").change(function () {
            $("#roomF").submit();
        });

        function postJson(params,url,Success){
            var pre = "/movie/";
            // var paramsJson = JSON.stringify(Params);
            url = pre + url;
            $.ajax({
                type: "post",
                url: url,
                data: params,
                dataType: "json",
                // contentType: "application/json;charset=utf-8",
                // dataType:"text",
                success: Success,
                // error: error,
            });
        }
        
    </script>
</html>