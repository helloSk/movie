<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="base.jsp"%>
<title>用户注册</title>
<style>
body {
	background: url(img/login.jpg);
	background-repeat: no-repeat;
	background-size: 100%;
	background-attachment: fixed;
}
@media (max-width: 768px){
 body{
 	background: none;
 }
}
</style>
<div class="container">
	<div class="row clearfix">
		<div class="col-sm-12 col-xs-12 loginhead">
			<div class="col-sm-5 col-xs-12 loginheadp"></div>
		</div>
	</div>
</div>
<!-- 内容 -->
<div class="container " id="loginbody">
	<div class="row logintop">
		<div class="col-md-12 col-sm-12 col-xs-12 ">
			<div class="row">
				<div class="col-md-8 col-sm-0 hidden-xs"></div>
				<div class="col-md-3 col-sm-12 col-xs-12"
					style="background-color: white;">
					<div class="logintitle">用户注册</div>
					<div class="col-sm-11 userlogin">
						<span class="message loginmessage">${actionMessages[0]}</span>
					</div>
					<form class="form-horizontal" action="user/register.do"
						method="post">
						<div class="input-group input-group-sm col-sm-10 col-xs-10 fgre">
							<span class="input-group-addon loginlable">用户名</span> <input
								type="text" class="form-control" id="username" name="username"
								placeholder="请输入用户名" value="${actionMessages[1]}">
						</div>
						<div class="input-group input-group-sm col-sm-10 col-xs-10 fgre">
							<span class="input-group-addon loginlable">密码</span> <input
								type="password" class="form-control" id="password"
								name="password" placeholder="请输入密码">
						</div>
						<div class="input-group input-group-sm col-sm-10 col-xs-10 fgre">
							<span class="input-group-addon loginlable">确认密码</span> <input
								type="password" class="form-control" id="repassword"
								name="repassword" placeholder="请确认密码">
						</div>
						<div class="input-group input-group-sm col-sm-10 col-xs-10 fgre">
							<span class="input-group-addon loginlable">联系方式</span> <input
								type="text" class="form-control" id="telephone"
								name="telephone" placeholder="请输入手机号" onkeyup="value=value.replace(/[^\d]/g,'')">
						</div>
						<div class="input-group col-sm-10 col-xs-10 rebut">
							<span class="input-group-addon loginlable"></span>
							<button type="button" id="registButton" class="btn loginbutton">注册</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="height: 15px"></div>
</body>
<script type="text/javascript" src="js/sts-site.js"></script>
</html>